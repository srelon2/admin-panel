<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Roles extends Model {
    use SoftDeletes;

    protected $table = 'roles';
    protected $dates = ['deleted_at'];
}
