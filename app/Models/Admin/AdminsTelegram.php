<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AdminsTelegram extends Model
{
    use SoftDeletes;
    //
    protected $table = 'admins_telegram';
    protected $dates = ['deleted_at'];
}
