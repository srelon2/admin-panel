<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    use SoftDeletes;
    //
    protected $table = 'routes';
    protected $dates = ['deleted_at'];

    public function getController(){
        return $this->belongsTo('App\Models\Admin\Controllers', 'controller');
    }
}
