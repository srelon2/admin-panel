<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supports extends Model
{
    use SoftDeletes;

    protected $table = 'supports';
    protected $dates = ['deleted_at'];
}
