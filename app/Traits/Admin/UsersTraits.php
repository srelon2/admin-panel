<?php

namespace App\Traits\Admin;
use App\User;

trait UsersTraits {

    public function getUsers() {
        return User::get();
    }
    public function getUser($key, $type= 'id') {
        return User::withTrashed()->where($type, $key)->first();
    }

    public function searchUsers($searchValue , $post= false, $table= false) {
        $items= User::where(function ($query) use ($searchValue){
            $query->where('id', '=', $searchValue)
                ->orwhere('name', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('email', 'LIKE', '%' . $searchValue . '%');
        });
        if($post) {
            if($post['order'][0]['dir']=='asc') {
                $items= $items->oldest($post['table'][$post['order'][0]['column']]);
            } else {
                $items= $items->latest($post['table'][$post['order'][0]['column']]);
            }
            $data= [
                'total'=> $items->count(),
                'data'=> $items->skip($post['start'])->take($post['length'])->get()
            ];
            return $data;
        } else {
            return $items->get();
        }
    }

    public function ajaxTableUsers($post) {
        $post['table']= [
            'id', 'name', 'email',
        ];
        $items= $this->searchUsers($post['search']['value'], $post);

        $data= array();
        foreach ($items['data'] as $item) {
            $data[]= [
                $item->id,
                $item->name,
                $item->email,
                "<div class='pull-right'>
                    <a href='#' class='btn btn-danger btn-sm sa-warning' data-id='".$item->id."'><i class=\"fa fa-trash\"></i></a>
                    <a href=".route('admin.users.info', ['id'=> $item->id])." class='btn btn-primary btn-sm'><i class=\"fa fa-edit mg-r-10\"></i>Редактировать</a>
                </div>",
            ];
        }
//        if(isset($post['order'][0]['dir'])) {
//            if($post['order'][0]['dir']=='asc') {
//                $data= $data->sortByDesc(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            } else {
//                $data= $data->sortBy(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            }
//        };
        ## Response
        return array(
            "iTotalRecords" => $items['total'],
            "iTotalDisplayRecords" => $items['total'],
            "aaData" => $data
        );
    }

    public function updateUser($data, $id) {
        if($id) {
            $item= User::find($id);
            $mess= 'изменен';
        } else {
            $item= new User;
            $mess= 'создан';
        }
        if(isset($data['password'])) {
            $item->password= bcrypt($data['password']);
        }
        unset($data['password']);
        foreach ($data as $key=> $value) {
            $item->$key= $value;
        }

//        $item->save()
        if($item->save()) {
            $data= [
                'mess'=>'Пользователь успешно '.$mess,
                'status'=> 'success',
            ];
            $this->saveHistory($item, 'admins', (($id) ? 'Изменил' : 'Создал').' Пользователь ID: '.$item->id);
        } else {
            $data= [
                'mess'=>'Пользователь не был '.$mess,
                'status'=> 'error',
            ];
        }
        $data['item']= $item;
        return $data;
    }
    public function actionUsers($action, $id) {
        $item= User::withTrashed()->find($id);

        try {
            switch ($action) {
                case 'delete':
                    $item->delete();
                    $mess= 'Добавлено в корзину';
                    break;
                case 'forceDelete':
                    $item->forceDelete();
                    $mess= 'Удалено с базы';
                    break;
                default:
                    $item->restore();
                    $mess= 'Востанновлено с корзины';
            }
            $item->save();
            $data= [
                'status'=> 'success',
                'mess'=> $mess
            ];
            return $data;
        } catch(Exception $e) {
            $data= [
                'status'=> 'error',
                'mess'=> $e
            ];
            return $data;
        }
    }
}
