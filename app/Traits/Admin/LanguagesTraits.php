<?php

namespace App\Traits\Admin;

use App\Models\Languages;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Rap2hpoutre\FastExcel\FastExcel;
use PHPUnit\Runner\Exception;

trait LanguagesTraits {

    public function getLanguages() {
        return Languages::get();
    }
    public function getLanguage($key, $type= 'id') {
        return Languages::where($type, $key)->first();
    }
    public function getLangs() {
        return Languages::pluck('key');
    }

    public function searchLanguages($searchValue , $post= false, $table= false) {
        $items= Languages::where(function ($query) use ($searchValue){
            $query->where('id', '=', $searchValue)
                ->orwhere('key', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('name', 'LIKE', '%' . $searchValue . '%');
        });
        if($post) {
            if($post['order'][0]['dir']=='asc') {
                $items= $items->oldest($post['table'][$post['order'][0]['column']]);
            } else {
                $items= $items->latest($post['table'][$post['order'][0]['column']]);
            }
            $data= [
                'total'=> $items->count(),
                'data'=> $items->skip($post['start'])->take($post['length'])->get()
            ];
            return $data;
        } else {
            return $items->get();
        }
    }

    public function ajaxTableLanguages($post) {
        $post['table']= [
            'id', 'name', 'email', 'role_id',
        ];
        $items= $this->searchLanguages($post['search']['value'], $post);
        $accessRole=$this->accessesRoles($this->admin, 'AdminRolesController')['view'];

        $data= array();
        foreach ($items['data'] as $item) {
            $data[]= [
                $item->id,
                $item->key,
                $item->name,
                "<a href=".route('admin.languages.info', ['id'=> $item->id])." class='btn btn-primary btn-sm pull-right'><i class=\"fa fa-edit mg-r-10\"></i>Редактировать</a>",
            ];
        }
//        if(isset($post['order'][0]['dir'])) {
//            if($post['order'][0]['dir']=='asc') {
//                $data= $data->sortByDesc(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            } else {
//                $data= $data->sortBy(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            }
//        };
        ## Response
        return array(
            "iTotalRecords" => $items['total'],
            "iTotalDisplayRecords" => $items['total'],
            "aaData" => $data
        );
    }

    public function updateLanguage($data, $id) {
        if($id) {
            $item= Languages::find($id);
            $key= $item->key;
            $mess= 'изменен';

        } else {
            $item= new Languages;
            $key= $data['key'];
            $mess= 'создан';
            $translation= File::get(resource_path('/lang/original.json'));
            File::put(resource_path('/lang/'.$key.'.json'), $translation);
        }
        $file=$key.'.json';
        $path= resource_path('/lang/'.$file);

        if(isset($data['restored'])) {
            $translation= collect(json_decode(File::get($path)));
            $translation= $translation->merge(json_decode($data['restored']));
            File::put($path, $translation);
            unset($data['restored']);
        }

        foreach ($data as $key=> $value) {
            $item->$key= $value;
        }

//        $item->save()
        if($item->save()) {
            $data= [
                'mess'=>'Перевод успешно '.$mess,
                'status'=> 'success',
            ];
            $translation= File::get($path);
            $item->translation= $translation;
            $this->saveHistory($item, 'languages', (($id) ? 'Изменил' : 'Создал').' перевод ID: '.$item->id);
        } else {
            $data= [
                'mess'=>'Перевод не был '.$mess,
                'status'=> 'error',
            ];
        }
        $data['item']= $item;

        if(empty($id)) {
            $lang= $item->key;

            Schema::table('pages', function($table) use ($lang)
            {
                $table->string('title_'.$lang);
                $table->text('desc_'.$lang)->nullable();
                $table->text('content_'.$lang)->nullable();
                $table->string('meta_title_'.$lang)->nullable();
                $table->string('meta_desc_'.$lang)->nullable();
                $table->string('meta_key_'.$lang)->nullable();
            });
        }

        return $data;
    }
    function updateTranslate($data, $id, $type){
        try {
            $item= Languages::find($id);
            $key= $data['key'];
            $value= $data['value'];

            $file=$item->key.'.json';
            $path= resource_path('/lang/'.$file);
            $translation= json_decode(File::get($path));
            $translation->$key= $value;
            File::put($path, json_encode($translation, JSON_UNESCAPED_UNICODE));

            $update[$key]= $value;
            $item->translation=json_encode($update);
            $this->saveHistory($item, 'languages', 'Изменил перевод Ключа: '.$key);

            return [
                'status'=> 'success',
                'mess'=> 'Успешно изменено'
            ];

        } catch (Exception $e) {
            return [
                'status'=> 'error',
                'mess'=> $e
            ];
        }
    }
    function importTranslate($data, $id, $type) {
        try {
            $item= Languages::find($id);
            $file=$item->key.'.json';
            $path= resource_path('/lang/'.$file);
            $translation= collect(json_decode(File::get($path)));

            switch ($type) {
                case 'excel':
                    $import= collect();
                    (new FastExcel)->import($data->file('import_file'), function ($line) use($import){
                        $import->put($line['key'], $line['translate']);
                    });
                    $translation= $translation->merge($import);
                    File::put($path, $translation);

                    $item->translation= $translation;
                    $this->saveHistory($item, 'languages', 'Изменил перевод ID: '.$item->id);
                    break;
            }
            return [
                'status'=> 'success',
                'mess'=> 'Успешно изменено'
            ];

        } catch (Exception $e) {
            return [
                'status'=> 'error',
                'mess'=> $e
            ];
        }

    }
    function createdKeyTranslate($data) {
        try {
            $original= json_decode(File::get(resource_path('/lang/original.json')));
            if(array_key_exists($data, $original)) return [
                'status'=> 'danger',
                'mess'=> 'Данный ключ уже создан'
            ];

            $dir  = scandir(resource_path("lang/"), 1);
            foreach ($dir as $key=> $file) {
                if(!strpos($file, 'json')) continue;

                $translate= json_decode(File::get(resource_path('lang/'.$file)));
                $translate->$data= '';
                File::put(resource_path('lang/'.$file), json_encode($translate, JSON_UNESCAPED_UNICODE));
            }

            $this->saveHistory(false, 'languages', 'Добавил новый ключ перевода : '.$data);
            return [
                'status'=> 'success',
                'mess'=> 'Ключ успешно создан'
            ];
        } catch (Exception $e) {
            return [
                'status'=> 'danger',
                'mess'=> 'Не удалось добавить ключ'
            ];
        }
    }
}
