<?php

namespace App\Traits\Admin;
use Route;

trait AdminNavigation {
    public function navigation($admin, $route) {

        $navigation= collect([
            ['title' => 'Dashboard',
            'icon' => 'fa fa-dashboard',
            'url' => route('admin.home'),
            'accessLogic' => 'AdminController'
            ],
            ['title' => 'Контент',
                'icon' => 'fa fa-keyboard-o',
                'pages' => [
                    [
                        'icon' => 'fa fa-file-text',
                        'title' => 'Внутренние страницы',
                        'url' => route('admin.pages'),
                        'accessLogic' => 'AdminPagesController'
                    ],
                    [
                        'icon' => 'fa fa-language',
                        'title' => 'Переводы',
                        'url' => route('admin.languages'),
                        'accessLogic' => 'AdminLanguagesController'
                    ],
                ]
            ],
            ['title' => 'Mailbox',
                'icon' => 'fa fa-envelope',
                'url' => route('admin.supports'),
                'accessLogic' => 'AdminSupportsController'
            ],
            ['title' => 'Администраторы',
                'icon' => 'fa fa-user-secret',
                'pages' => [
                    [
                        'icon' => 'fa fa-user-plus',
                        'title' => 'Администраторы',
                        'url' => route('admin.administrators'),
                        'accessLogic' => 'AdminAdministratorsController'
                    ],
                    [
                        'icon' => 'fa fa-unlock-alt',
                        'title' => 'Роли',
                        'url' => route('admin.roles'),
                        'accessLogic' => 'AdminRolesController'
                    ],
                ]
            ],
            ['title' => 'Пользователи',
                'icon' => 'fa fa-users',
                'pages' => [
                    [
                        'icon' => 'fa fa-users',
                        'title' => 'Пользователи',
                        'url' => route('admin.users'),
                        'accessLogic' => 'AdminUsersController'
                    ],
                ]
            ],
            ['title' => 'Настройки',
                'icon' => 'fa fa-cogs',
                'pages' => [
                    [
                        'icon' => 'fa fa-code-fork',
                        'title' => 'Routes',
                        'url' => route('admin.routes'),
                        'accessLogic' => 'AdminRoutesController'
                    ],
                ]
            ],
            ['title' => 'Logs',
                'icon' => 'fa fa-history',
                'url' => route('admin.logs'),
                'function'=> 'AdminLogsController@index',
                'accessLogic' => 'AdminLogsController'
            ],
            ['title' => 'Debugs',
                'icon' => 'fa fa-bug',
                'url' => route('admin.debugs'),
                'function'=> 'AdminLogsController@debugs',
                'accessLogic' => 'AdminLogsController'

            ],
        ]);
        $str= strpos($route, '@');
        $controller=substr($route, 0, $str);

        $navigation->transform(function($query,$key) use ($admin, $controller, $route){
            if(empty($query['accessLogic']) || $this->accessesRoles($admin, $query['accessLogic'])['view']) {
                if(isset($query['pages'])) {
                    $query['pages']= collect($query['pages'])->transform(function($page, $key) use($admin, $controller, $route) {
                        if(empty($page['accessLogic']) || $this->accessesRoles($admin, $page['accessLogic'])['view']) {
                            if((isset($page['function']) && $page['function']==$route) || empty($page['function']) && $page['accessLogic']==$controller) {
                                $page['isActive'] = true;
                            }
                            return $page;
                        }
                    })->filter();
                    if($query['pages']->where('isActive', true)->count()) {
                        $query['isActive']= true;
                        $query['isSubActive']= true;
                    }
                } elseif((isset($query['function']) && $query['function']==$route) || empty($query['function']) && $query['accessLogic']==$controller) {
                    $query['isActive']= true;
                }

                if(isset($query['url']) || count($query['pages'])) {
                    return $query;
                }
            }
        });
        return $navigation->filter();
    }


    public function getNavigation($admin) {
        $controller= str_replace('App\Http\Controllers\Admin\\', '',Route::currentRouteAction());
        $navigation= $this->navigation($admin, $controller);

        return $navigation;
    }



}


//['title' => 'Артефакты',
//    'url' => route('admin.home'),
//    'accessLogic' => function ()
//    {
//        return auth()->guard('admin')->user()->accessesRoles('UserVerification');
//    },],