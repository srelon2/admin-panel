<?php

namespace App\Traits\Admin;
use App\Models\Admin\Admin;
use Illuminate\Support\Facades\Hash;
use Google2FA;

use App\Mail\SendAdminAuthCode;
use Illuminate\Support\Facades\Mail;
use App\Traits\Admin\TelegramTrails;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


trait SettingsTraits {
    use TelegramTrails;

    public function updateSettings($data) {
        $item= $this->admin;
        if (empty($data['checked_password']) || Hash::check($data['checked_password'], $item->password)) {
            $submit= $data['submit'];
            unset($data['submit'], $data['checked_password']);

            switch ($submit) {
                case 'google_two_step':
                case 'email_two_step':
                case 'telegram_two_step':
                    $valid= $this->checkSecret($item, $data[$submit], $submit);
                    if($valid){
                        if($item->$submit == 1){
                            $item->$submit = 0;
                        }
                        else{
                            $item->$submit = 1;
                        }
                    } else {
                        $data= [
                            'status'=> $submit,
                            'mess'=> 'Неправильный код',
                            'submit'=> $submit
                        ];

                        return $data;
                    }

                    break;
                case 'chat_id':
                    $telegram= $this->updateAdminTelegram($data['chat_id']);
                    if($telegram) {
                        $item->chat_id= $telegram;
                    } else{
                        $data= [
                            'status'=> 'chat_id',
                            'mess'=> 'Chat ID уже используется',
                            'submit'=> 'telegram_two_step'
                        ];
                    }

                    break;
                default:
                    foreach ($data as $key=> $value) {
                        $item->$key= $value;
                    }
            }


            if($item->save()) {
                $data= [
                    'mess'=>'Настройки аккаунта успешно обновлены',
                    'status'=> 'success',
                ];
                $this->saveHistory($item, 'routes', 'Изменил свои настройки аккаунта');
            } else {
                $data= [
                    'mess'=>'Настройки аккаунта не были обновлены',
                    'status'=> 'error',
                ];
            }
            $data['submit']= $submit;
            return $data;


        } else {
            $data= [
                'status'=> 'checked_password',
                'mess'=> 'Пароли не совпадают',
            ];
            return $data;
        }
    }

    public function getTwoStep($id) {
        $item= Admin::find($id);

        if(is_null($item->google2fa_secret)){
            $google2fa = Google2FA::generateSecretKey();
            $item->google2fa_secret = $google2fa;
            $item->save();
        }
//        Google2FA::setAllowInsecureCallToGoogleApis(true);
        $data= [
            'google_two_step'=> $item->google_two_step,
            'google2fa_secret'=> $item->google2fa_secret,
            'email_two_step'=> $item->email_two_step,
            'telegram_two_step'=> $item->telegram_two_step,
        ];

        return $data;
    }

    public function secretSend($data, $type, $id= false) {
        if($id) {
            $item= Admin::find($id);
        } else {
            $item= $this->admin;
        }

        switch ($type) {
            case 'telegram_two_step':
                if($data) {
                    $chat_id= $this->getChatID($data['chat_id']);
                    $item->chat_id= $data['chat_id'];
                } else {
                    $chat_id= $this->getChatID($item->chat_id);
                }
                $api= $this->telegramApi();
                $secret= $this->generateRandomNumber();
                $api->sendMessage([
                    'chat_id' => $chat_id,
                    'text' => $secret,
                ]);
                $item->telegram_secret= $secret;
                $data= [
                    'mess'=>'Сообщение успешно отправлено',
                    'status'=> 'success',
                ];
                break;
            default:
                $item->email_secret= $this->generateRandomNumber();
                try {
                    Mail::to($item->email)->send(new SendAdminAuthCode($item));
                    $data= [
                        'mess'=>'Сообщение успешно отправлено',
                        'status'=> 'success',
                    ];
                }catch (\Exception $e){
                    $data= [
                        'mess'=>'Сообщение не отправлено',
                        'status'=> 'error',
                    ];
                }
        }

        $item->save();
        return $data;
    }

    public function clouseAdminSessions($id){
        try {

            if($id) {
                DB::table("admin_sessions")->where('user_id', Auth::guard('admin')->id())->where('id', '=', $id)->update(['payload'=> '', 'user_id'=>null]);
            } else {
                DB::table("admin_sessions")->where('user_id', Auth::guard('admin')->id())->update(['payload'=> '', 'user_id'=>null]);
            }

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getAdminSessions($id= false) {
        if(empty($id)) {
            $id= Auth::guard('admin')->id();
        }
        return DB::table("admin_sessions")->where('user_id', $id)->get();
    }
}
