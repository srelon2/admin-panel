<?php

namespace App\Traits;
use App\Models\Admin\Admin;
use App\User;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use Google2FA;

use App\Http\Controllers\Api\Telegram\Api;
use App\Models\Admin\TelegramBot;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Excel;
use Mockery\Exception;

trait GlobalTraits {

    public function translitSrt($str) {
        $tr = array(
            "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
            "Д"=>"d","Е"=>"e","Ё"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
            "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
            "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
            "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
            "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
            "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
            "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"e","ж"=>"j",
            "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
            "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
            "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
            "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
            "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
            ","=> "", "/"=> "_", " "=> '-'
        );
        return strtr($str,$tr);
    }
    public  function saveHistory($item, $file, $mess) {
//        $files= File::get(storage_path('admin/history/'.$file.'.json'));
//        dd(json_decode($files));
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {$ip=$_SERVER['HTTP_CLIENT_IP'];} elseif (!empty($_SERVER['HTTP_CF_CONNECTING_IP'])) {$ip=$_SERVER['HTTP_CF_CONNECTING_IP'];} else {$ip=$_SERVER['REMOTE_ADDR'];}
        $location = $this->ip_info($ip);
        $location = $location['city'].', '.$location['country'];
        $json= [
            'admin_id'=> $this->admin->id,
            'admin'=> $this->admin->name,
            'date'=> date('Y-m-d h:i:s'),
            'desc'=> $mess,
            'id'=> ($item) ? $item->id : false,
            'item'=> collect($item),
            'model'=> $file,
            'ip'=> $ip,
            'browser'=> request()->header('User-Agent'),
            'location' =>$location,
        ];
        if(file_exists(storage_path('admin/history/'.$file.'.json'))) {
            $logs= collect(json_decode(File::get(storage_path('admin/history/'.$file.'.json'))));
        }
        if(empty($logs)) {
            $logs= collect();
        }
        $logs->put(date('Y-m-d h:i:s'), $json);

        File::put(storage_path('admin/history/'.$file.'.json'), $logs);

        return true;
    }


    public function seedsRefresh($seeds, $table, $offset= 13) {
        try {
            $items= DB::table($table)->get();
            $file= file(database_path('/seeds/'.$seeds.'.php'));
            array_splice($file, $offset);
            File::put(database_path('/seeds/'.$seeds.'.php'), $file);

            foreach ($items as $item) {
                $str= '';
                foreach (collect($item)->except(['updated_at', 'created_at', 'deleted_at']) as $key=> $value) {
                    $str.="            '".$key."'=> '".$value."',\r\n";
                }
                $file= file(database_path('/seeds/'.$seeds.'.php'));
                $file[count($file)-1].='        $data[]= ['."\r\n".$str."        ];\r\n";
                file_put_contents( database_path('/seeds/'.$seeds.'.php'), $file );
            }
            $file= file(database_path('/seeds/'.$seeds.'.php'));
            $str='';
            $str.='        DB::table("'.$table.'")->insert($data);'."\r\n";
            $str.="    }\r\n";
            $str.="}";
            $file[count($file)-1].= $str;
            file_put_contents( database_path('/seeds/'.$seeds.'.php'), $file);

            return 'success';
        } catch (Exception $e) {
            return false;
        }
    }
    public function dbSeeds($seed) {
        try {
            Artisan::call('db:seed --class='.$seed);

            return 'success';
        } catch (Exception $e) {
            return false;
        }
    }
    public function generateRandomNumber($count= 6) {
        $a = '';
        for ($i = 0; $i<$count; $i++)
        {
            $a .= mt_rand(1,9);
        }
        return $a;
    }

    public function telegramApi($key='demo'){
        $token= TelegramBot::where('key', $key)->value('token');

        return new Api($token);
    }


    public function checkSecret($item, $secret, $type) {
        switch ($type) {
            case 'email_two_step':
            case 'telegram_two_step':
                $str=strpos($type, "_");
                $type_secret=substr($type, 0, $str).'_secret';

                if($secret==$item->$type_secret) {
                    $valid= true;
                    $item->$type_secret= $this->generateRandomNumber();
                    $item->save();
                }else {
                    $valid= false;
                }
                break;
            default:
                $valid = Google2FA::verifyKey($item->google2fa_secret, $secret, false);
        }
        return $valid;
    }
    public function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city"           => @$ipdat->geoplugin_city,
                            "state"          => @$ipdat->geoplugin_regionName,
                            "country"        => @$ipdat->geoplugin_countryName,
                            "country_code"   => @$ipdat->geoplugin_countryCode,
                            "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }
//
//    public function importTable($table, $format, $id) {
//        switch ($format) {
//            default:
//
//        }
//    }
}