<?php

namespace App\Providers;

use App\Extensions\DatabaseSessionHandler;
use App\Extensions\ManagerExtensions;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\ConnectionInterface;
use App\Http\Middleware\StartSessionExtensions;

use Config;

class SessionServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        $table   = Config::get('session.table');
        if(strripos($request->getRequestUri(), 'adminroute')) {
            $table= 'admin_'.$table;
        }
        $this->registerSessionManager($table);

        $this->registerSessionDriver();

        $this->app->singleton(StartSessionExtensions::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    protected function registerSessionManager($table)
    {
        $this->app->singleton('session', function ($app) use ($table) {
            return new ManagerExtensions($app, $table);
        });
    }
    protected function registerSessionDriver()
    {
        $this->app->singleton('session.store', function ($app) {
            // First, we will create the session manager which is responsible for the
            // creation of the various session drivers when they are needed by the
            // application instance, and will resolve them on a lazy load basis.
            return $app->make('session')->driver();
        });
    }
}