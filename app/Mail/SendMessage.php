<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMessage extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $item;

    public function __construct($item)
    {
        $this->item = $item;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            "hy" => $this->item->name,
            "msg" => $this->item->text
        ];

        return $this->from($this->item->email)->subject((isset($this->item->subject)) ? $this->item->subject : 'Служба поддержки')->markdown("emails.default_mail")->with($data);
    }
}