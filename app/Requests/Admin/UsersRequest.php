<?php

namespace App\Requests\Admin;

use App\Traits\Admin\RolesTraits;
use App\Requests\Request;
use Illuminate\Support\Facades\Auth;

class UsersRequest extends Request
{
    use RolesTraits;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $admin= Auth::guard('admin')->user();
        if($this->accessesRoles($admin,'AdminUsersController')['edit']) {
            return true;
        } else {
            return abort('403');
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $pass_required = 'required|min:3|max:100';
        if (isset($this->id)) {
            $pass_required = '';
        }
        //echo $id_str;exit;
        return [
            'name' => 'required|unique:users,name,' . $this->id . '|max:50',
            'email' => 'required|unique:users,email,' . $this->id . '|email|max:100',
            'password' => $pass_required,
        ];
    }

    public function messages()
    {
        return [
//            'name.required' => 'Please select page.',
        ];
    }

}
