<?php

namespace App\Requests;

use App\Requests\Request;

class SupportRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'text' => 'required',
        ];
    }

    public function messages()
    {
        return [
//            'name.required' => 'Please select page.',
        ];
    }

}
