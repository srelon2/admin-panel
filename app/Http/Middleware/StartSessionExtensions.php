<?php

namespace App\Http\Middleware;

use Illuminate\Session\Middleware\StartSession;
use Illuminate\Http\Request;

use App\SessionShare;

use Illuminate\Contracts\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Config;

class StartSessionExtensions extends StartSession
{
    protected $table;
    /**
     * Get the session implementation from the manager.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Session\SessionInterface
     */
    public function getSession(Request $request)
    {
        $session = $this->manager->driver();
        /**
         * Fallback to session in browser
         */
        $table   = Config::get('session.table');
        if(strripos($request->getRequestUri(), 'adminroute')) {
            $table= 'admin_'.$table;
        }
        $this->table = $table;

        if(!isset($session_id) || !$session_id)
                $session_id = $request->cookies->get($table);

        $session->setId($session_id);

        return $session;
    }

    protected function addCookieToResponse(Response $response, Session $session)
    {
        if ($this->sessionIsPersistent($config = $this->manager->getSessionConfig())) {
            $response->headers->setCookie(new Cookie(
                $this->table, $session->getId(), $this->getCookieExpirationDate(),
                $config['path'], $config['domain'], $config['secure'] ?? false,
                $config['http_only'] ?? true, false, $config['same_site'] ?? null
            ));
        }
    }
}