<?php

namespace App\Http\Controllers\Admin;

use App\Requests\Admin\PagesRequest;

use App\Traits\Admin\PagesTraits;
use App\Traits\Admin\LanguagesTraits;
use Illuminate\Support\Facades\Input;

class AdminPagesController extends Controller
{
    use PagesTraits, LanguagesTraits;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $accesses= $this->accessesRoles($this->admin,'AdminPagesController');

            if(!$accesses['view']) abort('403');
            $this->data['edit']= $accesses['edit'];

            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $this->data['items']= $this->getPages();
        return view('admin.pages', $this->data)->with('pagetitle', 'PagesTraits');
    }

    public function info($id= false, $backup= false){
        if($id) {
            if($backup) {
                $backup= $this->getBackup('pages', $id, base64_decode($backup));
            }

            if($backup){
                $this->data['item']= $backup;
            } else {
                $this->data['item']= $this->getPage($id);
            }
            $this->data['backups']= $this->getLog('pages', $id);
        }
        $this->data['languages']= $this->getLanguages();
        $this->data['roles']= $this->getRoles();
        return view('admin.pages-info', $this->data)->with('pagetitle', (($id) ? 'Edit Pages ID:'.$id : 'Create Pages').(($backup) ? ' Restored' : ''));
    }

    public function search() {
        $response= $this->ajaxTablePages($_POST);
        echo json_encode($response);
    }

    public function update(PagesRequest $request, $id= false) {
        $data= $request->except('_token');

        $response= $this->updatePage($data, $id);
        return redirect()->route('admin.pages.info', ['id'=>$response['item']->id])->with($response['status'], $response['mess']);
    }

    public function itemAction($action,$id= false) {
        if(empty($id)) {
            $id= Input::get('data');
        }
        $ajax= Input::get('ajax');
        $response= $this->actionPages($action, $id);
        switch ($response['status']) {
            case 'success':
                $msg= $response['mess'];
                $status= 'success';
                break;
            default:
                $msg= 'Error. Не удалось изменить! '.json_encode($response['mess']);
                $status= 'danger';
        }
        if($ajax) {
            return response()->json(array('msg'=> $msg, 'status'=>$status));
        } else {
            return redirect()->back()->with($status, $msg);
        }

    }
}
