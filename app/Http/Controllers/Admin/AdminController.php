<?php

namespace App\Http\Controllers\Admin;

use Route;

use App\Http\Controllers\Api\QrCode\ErrorCorrectionLevel;
use App\Http\Controllers\Api\QrCode\QrCode;

use GuzzleHttp;

class AdminController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Create a basic QR code
        $qrCode = new QrCode('dsasdasad');
        $qrCode->setEncoding('UTF-8');
        $qrCode->setErrorCorrectionLevel(new ErrorCorrectionLevel(ErrorCorrectionLevel::HIGH));
        $qrCode->setLogoPath(asset('logo_gr.png'));

        imagepng($qrCode->writeString('ellipse'), public_path('images/qrcode/1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX11F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX1.png'));
        $this->data['qr']= view('qr-table')->with($this->data)->render();
        return view('admin.dashboard')->with($this->data);
    }

    public function import($table, $format='excel', $id=false) {
        $responce= $this->importTable($table, $format, $id);
    }
}
