<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class AdminLogsController extends Controller
{
    protected $request;
    //

    public function __construct() {
        parent::__construct();

        $this->request = app('request');
    }

    public function index() {
        $this->data['items']= $this->getLogs();
        return view('admin.logs', $this->data)->with('pagetitle', 'Admin Logs');
    }

    public function debugs($type= 'new'){

        if($type=='new') {
            $this->data['logs']= $this->getDebugs();
        } else {
            $files= $this->getFiles(true, $type);
            $this->data['files']= $files;
            if ($this->request->input('log')) {
                $this->setFile(base64_decode($this->request->input('log')));
            }
            if ($this->request->input('download')) {
                return $this->download($this->pathToLogFile(base64_decode($this->request->input('download'))));
            }
            $this->data['logs']= $this->getBugs();
            $this->data['current_file'] = $this->getFileName();
        }
        $this->data['type']= $type;

        return view('admin.debugs', $this->data)->with('pagetitle', 'Debugs Logs');
    }
}


