<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Controllers;
use App\Models\Admin\Route;
use App\Requests\Admin\RoutesRequest;
use Illuminate\Support\Facades\Input;

use App\Traits\Admin\RoutesTraits;

class AdminRoutesController extends Controller
{
    use RoutesTraits;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();

        $this->middleware(function ($request, $next) {
            $accesses= $this->accessesRoles($this->admin,'AdminRoutesController');

            if(!$accesses['view']) abort('403');
            $this->data['edit']= $accesses['edit'];

            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
//        $items= Route::get();
//        foreach ($items as $item) {
//            $str= '';
//            foreach (collect($item)->except(['updated_at', 'created_at', 'deleted_at']) as $key=> $value) {
//                $str.="            '".$key."'=> '".$value."',\r\n";
//            }
//            $seeds= file(database_path('/seeds/admin/RoutesSeeder.php'));
//            $seeds[count($seeds)-4].='        $data[]= ['."\r\n".$str."        ];\r\n";
//            file_put_contents( database_path('/seeds/admin/RoutesSeeder.php'), $seeds );
//        }


        return view('admin.routes', $this->data)->with('pagetitle', 'Маршруты админ панели');
    }

    public function info($id= false, $backup= false){
        if($id) {
            if($backup) {
                $backup= $this->getBackup('routes', $id, base64_decode($backup));
            }

            if($backup){
                $this->data['item']= $backup;
            } else {
                $this->data['item']= $this->getRoute($id);
            }
            $this->data['backups']= $this->getLog('routes', $id);
        }
        $this->data['controllers']= $this->getRouteControllers();
        return view('admin.routes-info', $this->data)->with('pagetitle', (($id) ? 'Изменение маршрута ID:'.$id : 'Создание маршрута'));
    }

    public function search() {
        $response= $this->ajaxTableRoutes($_POST);
        echo json_encode($response);
    }

    public function update(RoutesRequest $request, $id= false) {
        $data= $request->except('_token');

        $response= $this->updateRoute($data, $id);
        return redirect()->route('admin.routes.info', ['id'=>$response['item']->id])->with($response['status'], $response['mess']);
    }

    public function refresh() {
        if(!$this->data['edit']) abort('403');

        $responseRoute= $this->seedsRefresh('admin/RoutesSeeder', 'routes');
        $responseControolers= $this->seedsRefresh('admin/ControllersSeeder', 'controllers');

        if($responseRoute && $responseControolers) {
            $status= 'success';
            $mess= 'Посев данных маршрутов успешно обновлен';
         } else {
            $status= 'error';
            $mess= 'Ошибка записи посева данных';
        }
        return redirect()->back()->with($status, $mess);
    }
    public function seeds() {
        $responseRoute= $this->dbSeeds('RoutesSeeder');
        $responseControolers= $this->dbSeeds('ControllersSeeder');

        if($responseRoute && $responseControolers) {
            $status= 'success';
            $mess= 'Маршруты успешно обновлены';
        } else {
            $status= 'error';
            $mess= 'Маршруты не были обновлены';
        }
        return redirect()->back()->with($status, $mess);
    }

    public function itemAction($action,$id= false) {
        if(empty($id)) {
            $id= Input::get('data');
        }
        $ajax= Input::get('ajax');
        $response= $this->actionRoutes($action, $id);
        switch ($response['status']) {
            case 'success':
                $msg= $response['mess'];
                $status= 'success';
                break;
            default:
                $msg= 'Error. Не удалось изменить! '.json_encode($response['mess']);
                $status= 'danger';
        }
        if($ajax) {
            return response()->json(array('msg'=> $msg, 'status'=>$status));
        } else {
            return redirect()->back()->with($status, $msg);
        }

    }
}
