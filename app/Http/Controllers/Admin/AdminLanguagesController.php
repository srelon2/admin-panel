<?php

namespace App\Http\Controllers\Admin;

use App\Requests\Admin\LanguagesRequest;
use File;
use App\Traits\Admin\LanguagesTraits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

class AdminLanguagesController extends Controller
{
    use LanguagesTraits;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();

        $this->middleware(function ($request, $next) {
            $accesses= $this->accessesRoles($this->admin,'AdminLanguagesController');

            if(!$accesses['view']) abort('403');
            $this->data['edit']= $accesses['edit'];

            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('admin.languages', $this->data)->with('pagetitle', 'LanguagesTraits');
    }

    public function info($id= false, $backup= false){
        if($id) {
            if($backup) {
                $backup= $this->getBackup('languages', $id, base64_decode($backup));
                $this->data['restored']= collect(json_decode($backup->translation));
            }
            $item= $this->getLanguage($id);
            $this->data['translation']= collect(json_decode(File::get(isset($item) ? resource_path('lang/'.$item->key.'.json') : resource_path('lang/en.json'))));
            $this->data['item']= $item;
            $this->data['backups']= $this->getLog('languages', $id);
        }
        $this->data['languages']= $this->getLanguages();
        return view('admin.languages-info', $this->data)->with('pagetitle', (($id) ? 'Edit Language ID:'.$id : 'Create Language').(($backup) ? ' Restored' : ''));
    }

    public function search() {
        $response= $this->ajaxTableLanguages($_POST);
        echo json_encode($response);
    }

    public function update(LanguagesRequest $request, $id= false) {
        $data= $request->except('_token');

        $response= $this->updateLanguage($data, $id);
        return redirect()->route('admin.languages.info', ['id'=>$response['item']->id])->with($response['status'], $response['mess']);
    }

    public function edit($id, $type= 'column') {
        $data= Input::get('data');
        $response= $this->updateTranslate($data, $id, $type);

        switch ($response['status']) {
            case 'success':
                $msg= $response['mess'];
                $status= 'success';
                break;
            default:
                $msg= 'Error. Не удалось изменить! '.json_encode($response['mess']);
                $status= 'danger';
        }
        return response()->json(array('msg'=> $msg, 'status'=>$status));
    }

    public function import(Request $request, $id, $type= 'excel') {
        if(!$this->data['edit']) abort('403');
        $validation= Validator::make(
            [
                'extension' => (isset($request->import_file)) ? strtolower($request->import_file->getClientOriginalExtension()) : null,
            ],
            [
                'extension'      => 'required|in:xlsx,xls',
            ]
        );
        if($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }

        $response= $this->importTranslate($request, $id, $type);

        switch ($response['status']) {
            case 'success':
                $msg= $response['mess'];
                $status= 'success';
                break;
            default:
                $msg= 'Error. Не удалось изменить! '.json_encode($response['mess']);
                $status= 'danger';
        }
        return redirect()->route('admin.languages.info', ['id'=>$id])->with($status, $msg);
    }

    public function createdKey(){
        if(!$this->data['edit']) abort('403');
        $data= Input::get('data');
        $response= $this->createdKeyTranslate($data);
        $msg= $response['mess'];
        $status= $response['status'];
        return response()->json(array('msg'=> $msg, 'status'=>$status));
    }
}
