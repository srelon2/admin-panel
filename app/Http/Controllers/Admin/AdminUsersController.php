<?php

namespace App\Http\Controllers\Admin;

use App\Requests\Admin\UsersRequest;
use App\Traits\Admin\UsersTraits;

use App\Http\Controllers\Admin\Controller;

class AdminUsersController extends Controller
{
    use UsersTraits;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            $accesses = $this->accessesRoles($this->admin, 'AdminUsersController');

            if (!$accesses['view']) abort('403');
            $this->data['edit'] = $accesses['edit'];

            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['items'] = $this->getUsers();

        return view('admin.users', $this->data)->with('pagetitle', 'Просмотр администраторов');
    }

    public function info($id = false, $backup = false)
    {
        if ($id) {
            if ($backup) {
                $backup = $this->getBackup('users', $id, base64_decode($backup));
            }

            if ($backup) {
                $this->data['item'] = $backup;
            } else {
                $this->data['item'] = $this->getUser($id);
            }
            $this->data['backups'] = $this->getLog('users', $id);
        }
        return view('admin.users-info', $this->data)->with('pagetitle', (($id) ? 'Edit Users ID:' . $id : 'Create Users') . (($backup) ? ' Restored' : ''));
    }

    public function search()
    {
        $response = $this->ajaxTableUsers($_POST);
        echo json_encode($response);
    }

    public function update(UsersRequest $request, $id= false) {
        $data= $request->except('_token');

        $response= $this->updateUser($data, $id);
        return redirect()->route('admin.users.info', ['id'=>$response['item']->id])->with($response['status'], $response['mess']);
    }
    public function itemAction($action,$id= false) {
        if(empty($id)) {
            $id= Input::get('data');
        }
        $ajax= Input::get('ajax');
        $response= $this->actionUsers($action, $id);
        switch ($response['status']) {
            case 'success':
                $msg= $response['mess'];
                $status= 'success';
                break;
            default:
                $msg= 'Error. Не удалось изменить! '.json_encode($response['mess']);
                $status= 'danger';
        }
        if($ajax) {
            return response()->json(array('msg'=> $msg, 'status'=>$status));
        } else {
            return redirect()->back()->with($status, $msg);
        }
    }
}