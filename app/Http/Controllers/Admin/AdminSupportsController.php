<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Requests\Admin\SendMessageRequest;

class AdminSupportsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();

        $this->middleware(function ($request, $next) {
            $accesses= $this->accessesRoles($this->admin,'AdminSupportsController');

            if(!$accesses['view']) abort('403');
            $this->data['edit']= $accesses['edit'];
            $this->data['adminPanel']= 'support';

            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type= 'inbox', $table= 'supports', $id= false){
//        dd($this->getSupports($type));
        $this->data['type']= $type;
        $this->data['table']= $table;
        $this->data['id']= $id;
        return view('admin.supports', $this->data)->with(['pagetitle'=> (($id) ? 'Ответы на сообщение ID: '.$id : 'Mailbox: '.ucfirst($type))]);
    }

    public function search($type= 'inbox', $table= 'supports', $id= false) {
        $response= $this->ajaxTableSupports($_POST, $type, $table, $id);
        echo json_encode($response);
    }
    public function action() {
        $data= Input::get('data');
        $table= Input::get('table');

        $response= $this->actionInbox($data, $table);
        switch ($response['status']) {
            case 'success':
                $msg= $response['mess'];
                $status= 'success';
                break;
            default:
                $msg= 'Error. Не удалось изменить! '.json_encode($response['mess']);
                $status= 'danger';
        }
        return response()->json(array('msg'=> $msg, 'status'=>$status));
    }
    public function itemAction($action,$id, $table= 'supports') {
        $response= $this->actionMessage($action, $id, $table);
        switch ($response['status']) {
            case 'success':
                $msg= $response['mess'];
                $status= 'success';
                break;
            default:
                $msg= 'Error. Не удалось изменить! '.json_encode($response['mess']);
                $status= 'danger';
        }
        return redirect()->back()->with($status, $msg);
    }
    public function compose($id= false, $answer= false){
        if($id) {
            if($answer) {
                $this->data['item']= $this->getMessage($id, 'supports', ['id','name', 'email']);
            } else {
                $this->data['item']= $this->getMessage($id, 'answers');
                if($this->data['item']->type=='answer' && $this->data['item']->draft) {
                    $answer= 'answer';
                }
            }
        }
        $this->data['answer']= $answer;
        $this->data['answer']= $answer;

        return view('admin.supports-compose', $this->data)->with(['pagetitle'=> (($answer) ? 'Ответ на сообщение ID: '.$id : 'Отправить новое сообщение')]);
    }

    public function send(SendMessageRequest $request, $id= false, $type=false) {
        $data= $request->except('_token');
        $response= $this->sendMessage($data, $id, $type);
        return redirect()->route('admin.support.compose')->with($response['status'], $response['mess']);
    }
    public function read($id, $table= 'supports') {
        $this->data['item']= $this->getMessage($id, $table);
        if(empty($this->data['item'])) abort('404');
        $this->data['table']= $table;

        return view('admin.supports-info', $this->data)->with(['pagetitle'=> (($table=='supports') ? 'Просмотр входящего сообщения ' : 'Просмотр исходящего сообщения').' ID: '.$id]);
    }
}
