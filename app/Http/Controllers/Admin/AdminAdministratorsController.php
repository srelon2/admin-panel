<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Admin;
use App\Requests\Admin\AdministratorsRequest;

use App\Traits\Admin\SettingsTraits;
use App\Traits\Admin\AdministratorsTraits;
use Illuminate\Support\Facades\Input;

class AdminAdministratorsController extends Controller
{
    use AdministratorsTraits, SettingsTraits;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            $accesses = $this->accessesRoles($this->admin, 'AdminAdministratorsController');

            if (!$accesses['view']) abort('403');
            $this->data['edit'] = $accesses['edit'];

            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['items'] = $this->getAdministrators();

        return view('admin.admins', $this->data)->with('pagetitle', 'Просмотр администраторов');
    }

    public function info($id = false, $backup = false)
    {
        if ($id) {
            if ($backup) {
                $backup = $this->getBackup('admins', $id, base64_decode($backup));
            }

            if ($backup) {
                $this->data['item'] = $backup;
            } else {
                $this->data['item'] = $this->getAdministrator($id);
            }
            $this->data['backups'] = $this->getLog('admins', $id);
        }
        $this->data['roles'] = $this->getRoles();
        return view('admin.admins-info', $this->data)->with('pagetitle', (($id) ? 'Edit administrators ID:' . $id : 'Create administrators') . (($backup) ? ' Restored' : ''));
    }

    public function search()
    {
        $response = $this->ajaxTableadministrators($_POST);
        echo json_encode($response);
    }

    public function update(AdministratorsRequest $request, $id= false) {
        $data= $request->except('_token');

        $response= $this->updateAdministrator($data, $id);
        return redirect()->route('admin.administrators.info', ['id'=>$response['item']->id])->with($response['status'], $response['mess']);
    }
    public function itemAction($action,$id= false) {
        if(empty($id)) {
            $id= Input::get('data');
        }
        $ajax= Input::get('ajax');
        $response= $this->actionAdministrators($action, $id);
        switch ($response['status']) {
            case 'success':
                $msg= $response['mess'];
                $status= 'success';
                break;
            default:
                $msg= 'Error. Не удалось изменить! '.json_encode($response['mess']);
                $status= 'danger';
        }
        if($ajax) {
            return response()->json(array('msg'=> $msg, 'status'=>$status));
        } else {
            return redirect()->back()->with($status, $msg);
        }
    }
    public function logoutOther($id= false) {
        if($this->clouseAdminSessions($id)) {
            return redirect()->back()->with('success', 'Вы успешно завершили остальные сеансы!');
        } else {
            return redirect()->back()->with('error', 'Сеансы не были завершины!');
        }
    }
}