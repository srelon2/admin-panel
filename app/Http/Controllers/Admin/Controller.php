<?php

namespace App\Http\Controllers\Admin;
use App\Traits\Admin\AdminNavigation;

use App\Traits\Admin\RolesTraits;
use App\Traits\GlobalTraits;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Traits\Admin\LogsTraits;
use App\Traits\Admin\SupportsTraits;

use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AdminNavigation, RolesTraits, GlobalTraits, LogsTraits, SupportsTraits;

    protected $admin;
    protected $data;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->data['admin']= $this->admin = Auth::guard('admin')->user();
            $this->data['navigation']= $this->getNavigation($this->admin);
            $this->data['adminPanel']= 'pages';
            $this->data['newMessages']= $this->getSupports(['view',0]);
            $accesses= $this->accessesRoles($this->admin,'AdminController');

            if(!$accesses['view'])  abort('403');

            return $next($request);
        });
    }
}
