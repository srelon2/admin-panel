<?php

namespace \Telegram\BotExceptions;

/**
 * Class TelegramUndefinedPropertyException.
 */
class TelegramUndefinedPropertyException extends \Exception
{
}
