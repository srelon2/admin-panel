<?php

namespace App\Http\Controllers\Api\Telegram\Exceptions;

/**
 * Class TelegramOtherException.
 */
class TelegramEmojiMapFileNotFoundException extends TelegramSDKException
{
}
