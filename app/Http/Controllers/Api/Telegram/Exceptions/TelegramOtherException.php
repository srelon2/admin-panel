<?php

namespace App\Http\Controllers\Api\Telegram\Exceptions;

/**
 * Class TelegramOtherException.
 */
class TelegramOtherException extends TelegramSDKException
{
}
