<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use App\Traits\Admin\LogsTraits;

class Handler extends ExceptionHandler
{
    use LogsTraits;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {

        if ($exception instanceof \Illuminate\Session\TokenMismatchException) {
            return redirect()->back();
        }
        if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
            return redirect()->route('home');
        }

        if ($this->isHttpException($exception)) {
            $statusCode = $exception->getStatusCode();
            switch ($statusCode) {
                case '403':
//                    GlobalFunctions::ActionLogs(Auth::guard('admin')->user(), 'Error 403 '.Route::getFacadeRoot()->current()->uri(), 'error 403');
                    return response()->view('errors/error', ['code'=>'403', 'msg'=>'Доступ к странице, которую Вы ищите, заблокирован', 'error'=>'Доступ заблокирован!','url'=>route('admin.home'), 'url_title'=> 'Dashboard']);
                    default:
                    return response()->view('errors/error', ['code'=>'404', 'msg'=>'Не удалось найти страницу', 'error'=>'Страница не найдена!','url'=>route('admin.home'), 'url_title'=> 'Dashboard']);
            }
        }
        if(strpos( $exception->getFile(),'UrlGenerator') !== false) {
            return response()->view('errors/error-routes', ['code'=>'404', 'msg'=>'Не удалось найти страницу', 'error'=>'Страница не найдена!','url'=>route('admin.home'), 'url_title'=> 'Dashboard']);
        }
        if($exception->getMessage() && $exception->getFile() && get_class($exception)!= 'Illuminate\Validation\ValidationException') {
            $this->saveDebug($request, $exception);
//            GlobalFunctions::SendBotMsg(urlencode("Error Debug - ".$exception->getMessage()." Detail: Exception - ".get_class($exception).". File - ".$exception->getFile().". Line - ".$exception->getLine()));
        }

        return parent::render($request, $exception);
    }
}
