<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="{{asset('admin')}}/images/favicon.ico">

        <title>Error {{$code}}</title>

        <!-- Bootstrap 4.0-->
        <link rel="stylesheet" href="{{asset('admin')}}/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">

        <!-- Bootstrap extend-->
        <link rel="stylesheet" href="{{asset('admin')}}/main/css/bootstrap-extend.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('admin')}}/main/css/master_style.css">

        <!-- Fab Admin skins -->
        <link rel="stylesheet" href="{{asset('admin')}}/main/css/skins/_all-skins.css">
    </head>
    <body class="hold-transition">

    <section class="bg-img h-p100" style="background-image: url({{asset('Admin')}}/images/photo4.jpg);">
        <div class="container h-p100">
            <div class="row h-p100 align-items-center justify-content-center">
                <div class="col-lg-8 col-11">
                    <div class="error-page bg-white rounded">
                        <div class="error-content">
                            <div class="row h-p100 align-items-center bg-brick-dark bg-dark">
                                <div class="col-lg-6 col-md-12 bg-white text-dark">
                                    <div class="p-30">
                                        <h1 class="text-warning font-size-80 font-weight-700"> 404</h1>
                                        <h2>{{$code}}</h2>
                                        <h5>{!!$error!!}</h5>
                                        <p>{!!$msg!!} </p>
                                        <div class="mb-15">
                                            <a href="{{(isset($url)) ? $url : route('home')}}" class="btn btn-info btn-block margin-top-10">{{(isset($url_title)) ? $url_title : 'Back to dashboard'}}</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="error-content-inner text-center p-15">
                                        <h1 class="margin-top-0 headline"><i class="fa fa-warning text-warning"></i></h1>
                                        <h2>Search Here</h2>
                                        <h4 class="mb-15">Please check the URL and try again</h4>

                                        <form class="search-form mb-15">
                                            <div class="input-group">
                                                <input type="text" name="search" class="form-control rounded" placeholder="Search">

                                                <div class="input-group-btn">
                                                    <button type="submit" name="submit" class="btn btn-warning"><i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <!-- /.input-group -->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.error-content -->
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- jQuery 3 -->
    <script src="{{asset('admin')}}/assets/vendor_components/jquery/dist/jquery.min.js"></script>

    <!-- popper -->
    <script src="{{asset('admin')}}/assets/vendor_components/popper/dist/popper.min.js"></script>

    <!-- Bootstrap 4.0-->
    <script src="{{asset('admin')}}/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>


    </body>
</html>