@extends('admin.layouts.admin_layout')

@section('content')
    <div class="card pd-20 pd-sm-40">
        <div class="table-responsive">
            <table id="datatable1" class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Name</th>
                    <th>Controller</th>
                    <th><a class="btn btn-success pull-right btn-sm" href="{{route('admin.routes.info')}}"><i class="fa fa-plus mg-r-10"></i>Создать</a></th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <td class="ion-fitness">{{$item->id}}</td>
                        <td>{{$item->title}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->controller}}</td>
                        <td><a href="{{route('admin.routes.info', ['id'=>$item->id])}}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-edit mg-r-10"></i>Редактировать</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- table-wrapper -->
    </div><!-- card -->

@endsection

@push('scripts')
<script>
    $(function(){

        $('#datatable1').DataTable({
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            bPaginate:true,
            responsive: false,
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            "ajax": {
                "url": "{{route('admin.routes.search')}}",
            },
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

    });
</script>

@endpush