@extends('admin.layouts.layout')

@push('topBtn')
@if(isset($item) && $edit)<a class="btn btn-success btn-sm mg-l-10" href="{{route('admin.pages.info')}}"><i class="fa fa-plus mg-r-5"></i> Добавить</a><a class="btn btn-primary btn-sm mg-l-10 btn-outline" data-toggle="modal" data-target="#backups" href="#"><i class="fa fa-history mg-r-5"></i> Бекапы</a>@endif
@endpush
@section('content')
    @if(isset($backups))
        <div id="backups" class="modal fade" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-lg wd-100p" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-header pd-x-20">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">История изменений</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body pd-20">
                        <div class="table-responsive">
                            <table id="table-log" class="table table-striped tx-14">
                                <thead>
                                <tr>
                                    <th>Admin</th>
                                    <th class="text-center">Date</th>
                                    <th>Action</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($backups as $key=>$backup)
                                    <tr>
                                        <td><a class="tx-inverse tx-14 tx-medium d-block" href="{{route('admin.administrators.info', ['id'=> $backup->admin_id])}}">{{$backup->admin}}</a> </td>
                                        <td class="text-center">{{$backup->date}}</td>
                                        <td class="wd-200">{{$backup->desc}}</td>
                                        <td><a href="{{route('admin.pages.info', ['id'=>$item->id, 'backup'=>base64_encode($key)])}}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-undo mg-r-10"></i> Восстановить</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div><!-- modal-dialog -->
        </div>
    @endif
    @if(count($errors))
        <div class="alert alert-danger mg-b-20" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <div class="d-flex align-items-center justify-content-start">
                <i class="icon ion-ios-close alert-icon tx-24"></i>
                <span>Ошибка валидации. Проверьте ввод на всех языках!</span>
            </div><!-- d-flex -->
        </div>
    @endif
    @if($edit)
        <form method="post" action="{{route('admin.pages.update', ['id'=>(isset($item) ? $item->id : '')])}}" enctype="multipart/form-data">
            @csrf
            @endif
            <div class="card box-body">
                <div>
                    <div class="form-group">
                        <label for="link">Url:</label>
                        <div class="input-group {{ $errors->has('url') ? 'error' : '' }}">
                            <input type="text" name="url" id="url" class="form-control" placeholder="Enter URL" value="{{(isset($item) ? $item->url : old('url') )}}" >
                            @if(isset($item))
                                <div class="input-group-prepend">
                                    <a class="btn btn-secondary" target="_blank" href="{{route('page', ['url'=>$item->url])}}">Перейти на страницу</a>
                                </div>
                            @endif
                        </div>
                        @if ($errors->has('url'))
                            <span class="help-block">
                                <ul class="alert"><li>{{ $errors->first('url') }}</li></ul>
                            </span>
                        @endif
                    </div>
                </div>
                <div>
                    <ul class="nav nav-tabs">
                        @foreach($languages as $key=>$lang)
                            <li class="nav-item">
                                <a class="nav-link bd-0 pd-y-8 @if(empty($key)) active @endif"  href="#{{$lang->key}}" aria-controls="{{$lang->key}}" role="tab" data-toggle="tab" aria-expanded="true">{{$lang->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tabcontent-border tab-content box-body" id="myTabContent">
                        @foreach($languages as $key=>$lang)
                            @php
                                $title= 'title_'.$lang->key;
                                $meta_title= 'meta_title_'.$lang->key;
                                $desc= 'desc_'.$lang->key;
                                $content= 'content_'.$lang->key;
                                $meta_desc= 'meta_desc_'.$lang->key;
                                $meta_key= 'meta_key_'.$lang->key;
                            @endphp
                            <div role="tabpanel" id="{{$lang->key}}" class="tab-pane @if(empty($key)) active @endif">
                                <div class="form-layout">
                                    <div class="form-group {{ $errors->has($title) ? ' error' : '' }}">
                                        <label for="link">Title: <span class="tx-danger">*</span></label>
                                        <input type="text" name="{{$title}}" id="{{$title}}" class="form-control" placeholder="Enter title" value="{{(isset($item) ? $item->$title : old($title) )}}" >
                                        @if ($errors->has($title))
                                            <span class="help-block">
                                                <ul class="alert"><li>{{ $errors->first($title) }}</li></ul>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has($desc) ? ' error' : '' }}">
                                        <label for="{{$desc}}" class="control-label">Description:</label>
                                        <textarea id="{{$desc}}"  name="{{$desc}}" class="form-control text-area">{{(isset($item) ? $item->$desc : old($desc) )}}</textarea>
                                        @if ($errors->has($desc))
                                            <span class="help-block">
                                                <ul class="alert"><li>{{ $errors->first($desc) }}</li></ul>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has($content) ? 'error' : '' }}">
                                        <label for="{{$content}}" class="control-label">Content:</label>
                                        <textarea id="{{$content}}"  name="{{$content}}" class="form-control text-area">{{(isset($item) ? $item->$content : old($content) )}}</textarea>
                                        @if ($errors->has($content))
                                            <span class="help-block">
                                                <ul class="alert"><li>{{ $errors->first($content) }}</li></ul>
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        <a class="btn btn-info btn-block rounded-0 btn-outline" href="#demo" data-toggle="collapse" aria-expanded="true">META ТЕГИ</a>
                                    </div>
                                    <div id="demo" class="collapse pd-20 bd bd-t-0">
                                        <div class="form-group {{ $errors->has($meta_title) ? ' error' : '' }}">
                                            <label for="link">Meta Title</label>
                                            <input type="text" name="{{$meta_title}}" id="{{$meta_title}}" class="form-control" placeholder="Enter title" value="{{(isset($item) ? $item->$meta_title : old($meta_title) )}}" >
                                            @if ($errors->has($meta_title))
                                                <span class="help-block">
                                                    <ul class="alert"><li>{{ $errors->first($meta_title) }}</li></ul>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group {{ $errors->has($meta_desc) ? ' error' : '' }}">
                                            <label for="{{$meta_desc}}" class="control-label">Meta Desc</label>
                                            <textarea id="{{$meta_desc}}"  name="{{$meta_desc}}" class="form-control text-area">{{(isset($item) ? $item->$meta_desc : old($meta_desc) )}}</textarea>
                                            @if ($errors->has($meta_desc))
                                                <span class="help-block">
                                                    <ul class="alert"><li>{{ $errors->first($meta_desc) }}</li></ul>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group {{ $errors->has($meta_key) ? ' error' : '' }}">
                                            <label for="{{$meta_key}}" class="control-label">Meta Key</label>
                                            <textarea id="{{$meta_key}}"  name="{{$meta_key}}" class="form-control text-area">{{(isset($item) ? $item->$meta_key : old($meta_key) )}}</textarea>
                                            @if ($errors->has($meta_key))
                                                <span class="help-block">
                                                    <ul class="alert"><li>{{ $errors->first($meta_key) }}</li></ul>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div><!-- card-body -->
                </div>
                <div class="form-layout-footer pd-20 pd-sm-20">
                    @if($edit)
                        <button type="submit" class="btn btn-info mg-r-5"><i class="fa fa-save mg-r-10"></i>Сохранить</button>
                    @endif
                    <a href="{{ route('admin.pages') }}" class="btn btn-secondary">Назад</a>
                </div>
            </div>
            @if($edit)
        </form>
    @endif
@endsection


@push('scripts')
<script src="{{asset('admin')}}/assets/vendor_components/ckeditor/ckeditor.js"></script>
<script>

    $(document).ready(function () {
        @foreach($languages as $lang)
            CKEDITOR.replace('{{$desc}}');
            CKEDITOR.replace('{{$content}}');
        @endforeach

        $('#table-log').DataTable({
            order: [[1, 'desc']],
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            "ajax": null,
        });
        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    });
</script>

@endpush