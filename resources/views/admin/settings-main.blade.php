<div class="col-lg-5 col-12">
    <form method="post" action="{{route('admin.settings.update')}}">
        @csrf
        <div class="box-header with-border">
            <h4>Редактирования профиля</h4>
        </div>
        <div class="box-body">
            <div class="form-group {{ $errors->has('name') ? ' error' : '' }}">
                <label for="name">Name</label>
                <div>
                    <input class="form-control" type="text" name="name" value="{{$admin->name}}" id="name">
                </div>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <ul class="alert"><li>{{ $errors->first('name') }}</li></ul>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('email') ? ' error' : '' }}">
                <label for="email">Email</label>
                <div>
                    <input class="form-control" type="email" name="email" value="{{$admin->email}}">
                </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <ul class="alert"><li>{{ $errors->first('email') }}</li></ul>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('gender') ? ' error' : '' }}">
                <label for="example-url-input">Пол</label>
                <div>
                    <select class="form-control" name="gender">
                        <option value="1" @if($admin->gender==0) selected @endif>Не указывать</option>
                        <option value="1" @if($admin->gender==1) selected @endif>Мужской</option>
                        <option value="2" @if($admin->gender==2) selected @endif>Женский</option>
                        <option value="3" @if($admin->gender==3) selected @endif>Другой</option>
                    </select>
                </div>
                @if ($errors->has('gender'))
                    <span class="help-block">
                        <ul class="alert"><li>{{ $errors->first('gender') }}</li></ul>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('birthday') ? ' error' : '' }}">
                <label for="date">День рождения</label>
                <div>
                    <input class="form-control" type="date" name="birthday" id="birthday" value="{{(isset($admin->birthday)) ? date('Y-m-d', strtotime($admin->birthday)) : ''}}">
                </div>
                @if ($errors->has('birthday'))
                    <span class="help-block">
                        <ul class="alert"><li>{{ $errors->first('birthday') }}</li></ul>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('checked_password') ? ' error' : '' }}">
                <label for="checked_password">Пароль для подтверждения</label>
                <div>
                    <input class="form-control" autocomplete="off" type="password" id="checked_password" name="checked_password">
                </div>
                @if ($errors->has('checked_password'))
                    <span class="help-block">
                        <ul class="alert"><li>{{ $errors->first('checked_password') }}</li></ul>
                    </span>
                @endif
            </div>
            {{--<div class="form-group">--}}
            {{--<label>Disabled Result</label>--}}
            {{--<select class="form-control select2" multiple="multiple" data-placeholder="Select a State" style="width: 100%;">--}}
            {{--<option selected="selected">Alabama</option>--}}
            {{--<option>Alaska</option>--}}
            {{--<option disabled="disabled">California (disabled)</option>--}}
            {{--<option>Delaware</option>--}}
            {{--<option>Tennessee</option>--}}
            {{--<option>Texas</option>--}}
            {{--<option>Washington</option>--}}
            {{--</select>--}}
            {{--</div>--}}
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-info mg-r-5" name="submit" value="main"><i class="fa fa-save mg-r-10"></i>Сохранить</button>
        </div>
    </form>
</div>