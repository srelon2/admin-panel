@extends('admin.layouts.login')

@section('content')
    <div class="login-box">
        <div class="login-box-body">
            <h3 class="text-center">Get started with Us</h3>
            <p class="login-box-msg">Sign in to start your session</p>

            <form class="form-horizontal login-form" role="form" novalidate="novalidate" method="POST" action="{{ route('admin.login.validate') }}">
                {{ csrf_field() }}
                <div class="form-group has-feedback {{ $errors->has('secret') ? 'error' : '' }}">
                    <input type="password" class="form-control rounded" name="secret" placeholder="Input your secret code">
                    <span class="ion ion-locked form-control-feedback"></span>
                    @if ($errors->has('secret'))
                        <span class="help-block">
                            <ul class="alert"><li>{{ $errors->first('secret') }}</li></ul>
                        </span>
                    @endif
                </div>
                <div class="row">
                    <!-- /.col -->
                    <!-- /.col -->
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-info btn-block margin-top-10">SIGN IN</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <div class="margin-top-30 text-center">
                <p>Не пришло сообщение? <a href="{{route('admin.auth.validate.resend', ['id'=> session('2fa:admin:id')])}}" class="text-warning ml-5">Выслать повторно</a></p>
            </div>

        </div>
        <!-- /.login-box-body -->
    </div>
@endsection