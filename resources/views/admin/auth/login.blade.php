@extends('admin.layouts.login')

@section('content')
    <div class="login-box">
        <div class="login-box-body">
            <h3 class="text-center">Get started with Us</h3>
            <p class="login-box-msg">Sign in to start your session</p>

            <form class="form-horizontal login-form" role="form" novalidate="novalidate" method="POST" action="{{ route('admin.login') }}">
                {{ csrf_field() }}
                <div class="form-group has-feedback {{ $errors->has('email') ? ' error' : '' }}">
                    <input type="email" class="form-control rounded" placeholder="Email" name="email" value="{{old('email')}}">
                    <span class="ion ion-email form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                        <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('email') }}</li></ul>
                    </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password') ? ' error' : '' }}">
                    <input type="password" class="form-control rounded" name="password" placeholder="Password">
                    <span class="ion ion-locked form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                        <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('password') }}</li></ul>
                    </span>
                    @endif
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-6">
                        <div class="fog-pwd">
                            <a href="javascript:void(0)" class="text-danger"><i class="ion ion-locked"></i> Forgot pwd?</a><br>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-info btn-block margin-top-10">SIGN IN</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <div class="margin-top-30 text-center">
                <p>Don't have an account? <a href="register.html" class="text-warning ml-5">Sign Up</a></p>
            </div>

        </div>
        <!-- /.login-box-body -->
    </div>
@endsection