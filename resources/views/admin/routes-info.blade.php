@extends('admin.layouts.layout')

@push('topBtn')
    @if(isset($item) && $edit)
        <a class="btn btn-success btn-sm mg-l-15" href="{{route('admin.routes.info')}}"><i class="fa fa-plus mg-r-5"></i> Новый маршрут</a><a class="btn btn-primary btn-sm mg-l-10 btn-outline" data-toggle="modal" data-target="#backups" href="#"><i class="fa fa-history mg-r-5"></i> Бекапы</a>
    @endif

    @if($edit)
        <div class="pull-right">
            <a href="{{route('admin.routes.seeds')}}" class="btn btn-outline-primary btn-sm"><i class="fa fa-download mg-r-10"></i> Скачать Seeds</a>
            <a href="{{route('admin.routes.refresh')}}" class="btn btn-outline-danger btn-sm"><i class="fa fa-refresh mg-r-10"></i> Обновить Seeds</a>
        </div>
    @endif
@endpush

@section('content')
    @if(isset($backups))
        <div id="backups" class="modal fade" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-lg wd-100p" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-header pd-x-20">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">История изменений</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body pd-20">
                        <div class="table-responsive">
                            <table id="table-log" class="table table-striped tx-14">
                                <thead>
                                <tr>
                                    <th>Admin</th>
                                    <th class="text-center">Date</th>
                                    <th>Action</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($backups as $key=>$backup)
                                    <tr>
                                        <td><a class="tx-inverse tx-14 tx-medium d-block" href="{{route('admin.administrators.info', ['id'=> $backup->admin_id])}}">{{$backup->admin}}</a> </td>
                                        <td class="text-center">{{$backup->date}}</td>
                                        <td class="wd-200">{{$backup->desc}}</td>
                                        <td><a href="{{route('admin.routes.info', ['id'=>$item->id, 'backup'=>base64_encode($key)])}}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-undo mg-r-10"></i> Восстановить</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div><!-- modal-dialog -->
        </div>
    @endif
    @if($edit)
    <form method="post" action="{{route('admin.routes.update', ['id'=>(isset($item) ? $item->id : '')])}}">
        @csrf
    @endif
        <div class="card pd-20 pd-sm-20">
        <div class="form-layout">
            <p class="mg-b-20 mg-sm-b-30">
                @if($edit)
                    @if(isset($item))Изменение маршрута Админ панели
                    @else
                        Создание нового маршрута Админ панели
                    @endif
                @else
                    Просмотр маршрута
                @endif
            </p>
            <div class="row">
                <div class="form-group col-lg-6 col-xl-1">
                    @php
                        $types= ['get', 'post'];
                    @endphp
                    <label for="type">Type: <span class="tx-danger">*</span></label>
                    <select class="form-control select2 {{ $errors->has('type') ? ' parsley-error' : '' }}" name="type" id='type'>
                        @foreach($types as $type)
                            <option @if((isset($item) && $type== $item->type) || old('type')==$type) selected="selected" @endif >{{$type}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('type'))
                        <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('type') }}</li></ul>
                        </span>
                    @endif
                </div>
                <div class="form-group col-lg-6 col-xl-3">
                    <label for="link">Link: <span class="tx-danger">*</span></label>
                    <input type="text" name="link" id="link" class="form-control {{ $errors->has('link') ? ' parsley-error' : '' }}" placeholder="Enter link" value="{{(isset($item) ? $item->link : old('link') )}}" >
                    @if ($errors->has('link'))
                        <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('link') }}</li></ul>
                        </span>
                    @endif
                </div>
                <div class="form-group col-lg-6 col-xl-5">
                    <label for="controller">Controller: <span class="tx-danger">*</span></label>

                    <div class="input-group grout-toggle">
                        <span class="input-group-addon bg-transparent">
                            <input id="add" name="add" value="1" type="checkbox" class="checkbox-toggle {{ $errors->has('controller') ? ' parsley-error' : '' }}">
                          <label class="ckbox mg-b-0" for="add">Add</label>
                        </span>
                        <select name="controller" class="form-control select2 container-select2 {{ $errors->has('controller') ? ' parsley-error' : '' }}" id='currency'>
                            @foreach($controllers as $controller)
                                <option @if((isset($item) && $controller->id== $item->controller) || old('controller')==$controller->id) selected="selected" @endif  value="{{$controller->id}}">{{$controller->name}}</option>
                            @endforeach
                        </select>
                        <input type="text" class="form-control form-toggle form-add d-none" placeholder="Enter controller" name="newController" value="{{(old('newController')==null) ? 'AdminController' : old('newController')}}">

                    </div>
                    @if ($errors->has('controller'))
                        <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('controller') }}</li></ul>
                        </span>
                    @endif
                    @if ($errors->has('newController'))
                        <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('newController') }}</li></ul>
                        </span>
                    @endif
                </div>
                <div class="form-group col-lg-6 col-xl-3">
                    <label for="function">Function: <span class="tx-danger">*</span></label>
                    <div class="input-group">
                        <span class="input-group-addon">@</span>
                        <input type="text" name="function" id="f
                        unction" class="form-control {{ $errors->has('function') ? ' parsley-error' : '' }}" placeholder="Enter function" value="{{(isset($item) ? $item->function : old('function') )}}" >
                    </div>
                    @if ($errors->has('function'))
                        <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('function') }}</li></ul>
                        </span>
                    @endif
                </div>
                <div class="form-group col-xl-6">
                    <label for="title">Title:</label>
                    <input type="text" name="title" id="title" class="form-control {{ $errors->has('title') ? ' parsley-error' : '' }}" placeholder="Enter title" value="{{(isset($item) ? $item->title : old('title') )}}" >
                    @if ($errors->has('title'))
                        <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('title') }}</li></ul>
                        </span>
                    @endif
                </div>

                <div class="form-group col-xl-6">
                    <label for="name">Name: <span class="tx-danger">*</span></label>
                    <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? ' parsley-error' : '' }}" placeholder="Enter name" value="{{(isset($item) ? $item->name : old('name') )}}" >
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="description" class="control-label">Description</label>
                <textarea id="description"  name="description" class="form-control text-area">
                        {{(isset($item) ? $item->description : old('description') )}}
                </textarea>
            </div>
        </div>
        <div class="form-layout-footer">
            @if($edit)
                <button type="submit" class="btn btn-info mg-r-5"><i class="fa fa-save mg-r-10"></i>Сохранить</button>
            @endif
            <a href="{{ route('admin.routes') }}" class="btn btn-secondary">Назад</a>
        </div>
    </div>
    @if($edit)
        </form>
    @endif
@endsection


@push('scripts')
    <script>

        $(document).ready(function () {
            $(".select2").select2();

            $(".checkbox-toggle").click(function(){toggleCheckbox()});
//            $('.container-select2').on('select2:select', function (e) {
//                $('.form-controller').val(e.params.data['id']);
//            });
//            $('.form-add').on('change', function () {
//                $('.form-controller').val(this.value);
//            });
            toggleCheckbox();
            CKEDITOR.replace('description');

            @if(!$edit)
                $('.form-control').prop('readonly', true);
                $('.select2').prop('disabled', true);
            @endif

            $('#table-log').DataTable({
                    order: [[1, 'desc']],
                    'aoColumnDefs': [{
                        'bSortable': false,
                        'aTargets': [-1] /* 1st one, start by the right */
                    }],
                    "ajax": null,
                });
                // Select2
                $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
        });

        function toggleCheckbox() {
            if($('.checkbox-toggle').prop('checked')) {
                $('.form-add').removeClass('d-none');
                $('.grout-toggle .select2').addClass('d-none');
            } else {
                $('.grout-toggle .select2').removeClass('d-none');
                $('.form-add').addClass('d-none');
            }
        }
    </script>

@endpush