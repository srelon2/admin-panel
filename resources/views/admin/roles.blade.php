@extends('admin.layouts.layout')

@push('topBtn')
@if($edit)<a class="btn btn-success btn-sm mg-l-10" href="{{route('admin.roles.info')}}"><i class="fa fa-plus mg-r-10"></i>Создать</a> @endif
@endpush

@section('content')
    <div class="box-body card">
        <div class="table-responsive">
            <table id="datatable1" class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Role</th>
                    <th></th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div><!-- table-wrapper -->
    </div><!-- card -->

@endsection

@push('scripts')
<script>
    $(function(){
        var dataTable= $('#datatable1').DataTable({
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            bPaginate:true,
            responsive: false,
            order: [[0, 'desc']],
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            "ajax": {
                "url": "{{route('admin.roles.search')}}",
            },
            "drawCallback": function () {
                //Warning Message
                $('.sa-warning').click(function(){
                    event= $(this);
                    swal({
                        title: "Вы уверены?",
                        text: "Роль будет помещена в корзину!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Да, удалить",
                        closeOnConfirm: false
                    }, function(){
                        $.ajax({
                            type:'get',
                            url: "{{route('admin.roles.item.action', ['action'=>'delete'])}}",
                            data: {
                                'data': $(event).data('id'),
                                'ajax': true,
                            },
                            headers: {
                                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                            },
                            success:function(data){
                                swal("Deleted!", data['msg'], data['status']);
                                dataTable.draw()
                                $('[data-toggle="tooltip"]').tooltip('dispose');
                            },
                            error: function(msg){
                                console.log(msg);
                                swal("Deleted!", 'Не удалось удалить запись.', 'error');
                            }
                        });


                    });
                });
            }
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

    });
</script>

@endpush