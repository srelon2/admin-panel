@extends('admin.layouts.layout')

@push('topBtn')
    @if(isset($item) && $edit)<a class="btn btn-success btn-sm mg-l-10" href="{{route('admin.administrators.info')}}"><i class="fa fa-plus mg-r-5"></i> Добавить</a><a class="btn btn-primary btn-sm mg-l-10 btn-outline" data-toggle="modal" data-target="#backups" href="#"><i class="fa fa-history mg-r-5"></i> Бекапы</a>@endif
@endpush

@section('content')
    @if(isset($backups))
        <div id="backups" class="modal fade" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-lg wd-100p" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-header pd-x-20">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">История изменений</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body pd-20">
                        <div class="table-responsive">
                            <table id="table-log" class="table table-striped tx-14">
                                <thead>
                                <tr>
                                    <th>Admin</th>
                                    <th class="text-center">Date</th>
                                    <th>Action</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($backups as $key=>$backup)
                                    <tr>
                                        <td><a class="tx-inverse tx-14 tx-medium d-block" href="{{route('admin.administrators.info', ['id'=> $backup->admin_id])}}">{{$backup->admin}}</a> </td>
                                        <td class="text-center">{{$backup->date}}</td>
                                        <td class="wd-200">{{$backup->desc}}</td>
                                        <td><a href="{{route('admin.administrators.info', ['id'=>$item->id, 'backup'=>base64_encode($key)])}}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-undo mg-r-10"></i> Восстановить</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div><!-- modal-dialog -->
        </div>
    @endif
    @if($edit)
        <form method="post" action="{{route('admin.administrators.update', ['id'=>(isset($item) ? $item->id : '')])}}">
            @csrf
            @endif
            <div class="card pd-20 pd-sm-20">
                <div class="form-layout">
                    <p class="mg-b-20 mg-sm-b-30">
                        @if($edit)
                            @if(isset($item))Изменение Адмнинистратора
                            @else
                                Создание нового Администратора
                            @endif
                        @else
                            Просмотр Администратора
                        @endif
                    </p>
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label for="link">Name: <span class="tx-danger">*</span></label>
                            <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? ' parsley-error' : '' }}" placeholder="Enter name" value="{{(isset($item) ? $item->name : old('name') )}}" >
                            @if ($errors->has('name'))
                                <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="Email">Email: <span class="tx-danger">*</span></label>
                            <input type="email" name="email" id="email" class="form-control {{ $errors->has('email') ? ' parsley-error' : '' }}" placeholder="Enter email" value="{{(isset($item) ? $item->email : old('email') )}}" >
                            @if ($errors->has('email'))
                                <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('email') }}</li></ul>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="Password">Password: <span class="tx-danger">*</span></label>
                            <input type="password" name="password" id="password" class="form-control {{ $errors->has('password') ? ' parsley-error' : '' }}" placeholder="Enter password" value="" >
                            @if ($errors->has('password'))
                                <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('password') }}</li></ul>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="role_id">Role: <span class="tx-danger">*</span></label>
                            <select name="role_id" class="form-control select2 container-select2 {{ $errors->has('role_id') ? ' parsley-error' : '' }}" id='currency'>
                                @foreach($roles as $role)
                                    <option @if((isset($item) && $role->id== $item->role_id) || old('role_id')==$role->id) selected="selected" @endif  value="{{$role->id}}">{{$role->role}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('role_id'))
                                <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('role_id') }}</li></ul>
                                </span>
                            @endif
                        </div>
                        @if(isset($item))
                            <div class="form-group col-lg-12">
                                <input id="confirmed" value="1" name="confirmed" type="checkbox" {{((isset($item) && $item->confirmed) || old('confirmed')) ? 'checked' : '' }}>
                                <label for="confirmed" class="ckbox">Confirmed</label>
                            </div>
                            <div class="form-group col-lg-12">
                                <input id="google_two_step" value="1" name="google_two_step" type="checkbox" {{((isset($item) && $item->google_two_step) || old('google_two_step')) ? 'checked' : '' }}>
                                <label class="ckbox" for="google_two_step">Google two step</label>
                            </div>
                            <div class="form-group col-lg-12">
                                <input value="1" id="email_two_step" name="email_two_step" type="checkbox" {{((isset($item) && $item->email_two_step) || old('email_two_step')) ? 'checked' : '' }}>
                                <label for="email_two_step" class="ckbox">Email two step</label>
                            </div>
                        @endif
                        <div class="form-group col-lg-12">
                            <input id="active" value="1" name="active" type="checkbox" {{((isset($item) && $item->active) || old('active')) ? 'checked' : '' }}>
                            <label for="active" class="ckbox">Active</label>
                        </div>
                    </div>
                </div>
                <div class="form-layout-footer">
                    @if($edit)
                        <button type="submit" class="btn btn-info mg-r-5"><i class="fa fa-save mg-r-10"></i>Сохранить</button>
                    @endif
                    <a href="{{ route('admin.administrators') }}" class="btn btn-secondary">Назад</a>
                </div>
            </div>
            @if($edit)
        </form>
    @endif
@endsection


@push('scripts')
<script>

    $(document).ready(function () {
        $(".select2").select2();

        $('#table-log').DataTable({
            order: [[1, 'desc']],
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            "ajax": null,
        });
        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    });
</script>

@endpush