@extends('admin.layouts.layout')

@push('topBtn')
    @if($edit && isset($item))<a class="btn btn-success btn-sm mg-l-10" href="{{route('admin.languages.info')}}"><i class="fa fa-plus mg-r-10"></i>Создать</a><a class="btn btn-primary btn-sm mg-l-10 btn-outline" data-toggle="modal" data-target="#backups" href="#"><i class="fa fa-history mg-r-5"></i> Бекапы</a>
    @endif
@endpush

@section('content')
    @if(isset($backups))
        <div id="backups" class="modal fade" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-lg wd-100p" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-header pd-x-20">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">История изменений</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body pd-20">
                        <div class="table-responsive">
                            <table id="table-log" class="table table-striped tx-14">
                                <thead>
                                <tr>
                                    <th>Admin</th>
                                    <th class="text-center">Date</th>
                                    <th>Action</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($backups as $key=>$backup)
                                    <tr>
                                        <td><a class="tx-inverse tx-14 tx-medium d-block" href="{{route('admin.administrators.info', ['id'=> $backup->admin_id])}}">{{$backup->admin}}</a> </td>
                                        <td class="text-center">{{$backup->date}}</td>
                                        <td class="wd-200">{{$backup->desc}}</td>
                                        <td><a href="{{route('admin.languages.info', ['id'=>$item->id, 'backup'=>base64_encode($key)])}}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-undo mg-r-10"></i> Восстановить</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div><!-- modal-dialog -->
        </div>
    @endif
    <div class="card pd-20 pd-sm-20">
        @if($edit)
            <form method="post" id="update" action="{{route('admin.languages.update', ['id'=>(isset($item) ? $item->id : '')])}}">
            @csrf
        @endif
        <div class="form-layout">
            <div class="mg-b-20 mg-sm-b-30">
                {{(isset($item)) ? 'Просмотр перевода' : 'Создание перевода'}}
                <div class="dropdown dropdown-sm d-inline mg-l-15">
                    <button class="btn btn-outline btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">{{(isset($item)) ? $item->name : 'New'}}</button>
                    <div class="dropdown-menu">
                        @foreach($languages as $language)
                            <a class="dropdown-item" href="{{route('admin.languages.info', ['id'=>$language->id])}}">{{$language->name}}</a>
                        @endforeach
                    </div>
                </div>

                @if($edit)
                    <div class="pull-right">
                        <button class="btn btn-info btn-sm"><i class="fa fa-save mg-r-10"></i>Сохранить</button>
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="form-group col-md-6 {{ $errors->has('key') ? 'error' : '' }}">
                    <label for="link">Key: <span class="tx-danger">*</span></label>
                    <input type="text" name="key" id="key" class="form-control" placeholder="Enter key" value="{{(isset($item) ? $item->key : old('key') )}}" @if(isset($item)) disabled @endif>
                    @if ($errors->has('key'))
                        <span class="help-block">
                        <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('key') }}</li></ul>
                    </span>
                    @endif
                </div>
                <div class="form-group col-md-6 {{ $errors->has('name') ? 'error' : '' }}">
                    <label for="name">Full Name: <span class="tx-danger">*</span></label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="Enter name" value="{{(isset($item) ? $item->name : old('name') )}}" >
                    @if ($errors->has('name'))
                        <span class="help-block">
                        <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
                    </span>
                    @endif
                </div>
            </div>
            @if(isset($restored))
                <input name="restored" type="hidden" class="restored" value="{{json_encode($restored)}}">
            @endif
        </div>
        @if($edit)
            </form>
            @if(isset($item))
                <form method="post" id="update" action="{{route('admin.languages.import', ['id'=>$item->id])}}"  enctype="multipart/form-data">
                    @csrf
                    <p>Вы можете загрузить готовый файл перевода, в формате Excel</p>
                    <div class="row mg-b-30">
                        <div class="col-xl-4 col-lg-6">
                            <div class="form-group {{ $errors->has('extension') ? ' error' : '' }}">
                                <div class="custom-file">
                                    <input type="file" id="importFile" class="custom-file-input" name="import_file">
                                    <label for="importFile" class="custom-file-label">Choose file...</label>
                                </div>
                                @if ($errors->has('extension'))
                                    <span class="help-block">
                                        <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('extension') }}</li></ul>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-6">
                            <button type="submit" class="btn btn-primary btn-sm btn-block"><i class="fa fa-download mg-r-10"></i>Import file</button>
                        </div>
                    </div>
                </form>
                <form onsubmit="createKey(this); return false">
                    <div class="input-group form-group">
                        <!-- /btn-group -->
                        <input type="text" class="form-control input-key" placeholder="Добавить новый ключ">
                        <div class="input-group-prepend">
                            <button type="submit" class="btn btn-success"><i class="fa fa-plus mg-r-10"></i>Добавить</button>
                        </div>
                    </div>
                </form>
            @endif
        @endif
        @if(isset($item))
            <div>
                <div class="table-responsive">
                    <table id="translation-table" class="table table-striped">
                        <thead>
                        <tr>
                            <th data-column="key">Id</th>
                            <th data-column="key">Key</th>
                            <th data-column="translate">Перевод (изменяемый)</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $i= 0;
                        @endphp
                        @foreach($translation as $key=> $translate)
                            @if(isset($restored[$key]) && $restored[$key]!=$translate)
                                <tr>
                                    <td>{{++$i}}</td>
                                    <td>{{$key}}</td>
                                    <td contentEditable='true' class='edit bg-pale-success' data-key="{{$key}}">{{$restored[$key]}}</td>
                                </tr>
                            @else
                                <tr>
                                    <td>{{++$i}}</td>
                                    <td>{{$key}}</td>
                                    <td contentEditable='true' class='edit' data-key="{{$key}}">{{$translate}}</td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
        <div class="form-layout-footer mg-t-30">
            @if($edit)
                <button type="submit" class="btn btn-info mg-r-5" form="update"><i class="fa fa-save mg-r-10"></i>Сохранить</button>
            @endif
            <a href="{{ route('admin.languages') }}" class="btn btn-secondary">Назад</a>
        </div>
    </div>
@endsection
@push('scripts')
<!-- Editable -->
@if(isset($item))
    <script src="{{asset('admin')}}/assets/vendor_components/tiny-editable/mindmup-editabletable.js"></script>
    <script src="{{asset('admin')}}/assets/vendor_components/tiny-editable/numeric-input-example.js"></script>
    <script>
        var restored= false;

        function updateFunction() {
            $('#translation-table').editableTableWidget({
                editor: $('<textarea>'),
            });
            $('table .edit').on('change', function() {
                key= $(this).data('key');
                value= $(this).text();
                data= {
                    'key': key,
                    'value': value
                };
                if(restored) {
                    restored[key]= value;
                    $('.restored').val(JSON.stringify(restored));
                }
                ajax(data, "{{route('admin.languages.edit', ['id'=>$item->id,'type'=>'column'])}}");
            });
        }
        $(function(){
            updateFunction();
            @if(isset($restored))
                restored= JSON.parse($('.restored').val());
            @endif

            $('#table-log').DataTable({
                order: [1, 'desc'],
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }],
                "ajax": null,
            });
            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
        });
        var dataTable= $('#translation-table').DataTable({
            order: [0, 'desc'],
            "ajax": null,
            dom: 'lBfrtip',
            buttons: [
                { extend: 'excel', className: 'btn-sm', 'text': 'Download Excel', 'customizeData': function (data) {
                    data.header[1]= 'key';
                    data.header[2]= 'translate';
                }, title: ''}
            ],
            "classes": {
                sWrapper: "dataTables_wrapper dt-bootstrap4",
                sLength: "dataTables_length mg-r-15",
            }
        });
        function ajax($data, $route, action=false, $type='post') {
            $.ajax({
                type:$type,
                url: $route,
                data: {
                    'data': $data,
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
//                    var msgBlock= $('.msg-block .alert-'+data['status']);
//                    msgBlock.removeClass('d-none').find('.align-items-center span').text(data['msg']);
                    if(data['status']=='success') {
                        $.toast({
                            heading: 'Alert. Success!',
                            text: data['msg'],
                            position: 'top-right',
                            loaderBg: '#ff6849',
                            icon: 'success',
                            hideAfter: 3500,
                            stack: 6
                        });
                        if(action=='createKey') {
                            var rowNode =dataTable.row.add([
                                dataTable.data().length+1,
                                $data,
                                ''
                            ]).draw().node();
                            $( rowNode ).find('td').eq(2).addClass('edit').attr('data-key', $data);
                            updateFunction();
                        }
                    } else {
                        $.toast({
                            heading: 'Alert. Error!',
                            text: data['msg'],
                            position: 'top-right',
                            loaderBg: '#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }


                    dataTable.draw();
                },
                error: function(msg){
                    console.log(msg);
                }
            });
        }
        function createKey($this) {
            $data= $($this).find('.input-key').val();
            $($this).find('.input-key').val('');
            ajax($data, "{{route('admin.languages.created.key')}}", 'createKey')
        }
    </script>
@endif
@endpush