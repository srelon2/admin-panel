@extends('admin.layouts.layout')

@push('topBtn')
@if(isset($item) && $edit)<a class="btn btn-success btn-sm mg-l-15" href="{{route('admin.roles.info')}}"><i class="fa fa-plus mg-r-5"></i> Новая роль</a><a class="btn btn-primary btn-sm mg-l-10 btn-outline" data-toggle="modal" data-target="#backups" href="#"><i class="fa fa-history mg-r-5"></i> Бекапы</a> @endif
@endpush

@section('content')
    @if(isset($backups))
        <div id="backups" class="modal fade" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-lg wd-100p" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-header pd-x-20">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">История изменений</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body pd-20">
                        <div class="table-responsive">
                            <table id="table-log" class="table table-striped tx-14">
                                <thead>
                                <tr>
                                    <th>Admin</th>
                                    <th class="text-center">Date</th>
                                    <th>Action</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($backups as $key=>$backup)
                                    <tr>
                                        <td><a class="tx-inverse tx-14 tx-medium d-block" href="{{route('admin.administrators.info', ['id'=> $backup->admin_id])}}">{{$backup->admin}}</a> </td>
                                        <td class="text-center">{{$backup->date}}</td>
                                        <td class="wd-200">{{$backup->desc}}</td>
                                        <td><a href="{{route('admin.roles.info', ['id'=>$item->id, 'backup'=>base64_encode($key)])}}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-undo mg-r-10"></i> Восстановить</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div><!-- modal-dialog -->
        </div>
    @endif
    @if($edit)
    <form method="post" action="{{route('admin.roles.update', ['id'=>(isset($item) ? $item->id : '')])}}">
        @csrf
    @endif
        <div class="card pd-20 pd-sm-20">
        <div class="form-layout">
            <p class="mg-b-20 mg-sm-b-30">
                @if($edit)
                    @if(isset($item))Изменение роли Администратора
                    @else
                        Создание новой роли Администратора
                    @endif
                @else
                    Просмотр роли Администратора
                @endif
            </p>
            <div class="form-group">
                <label for="link">Role: <span class="tx-danger">*</span></label>
                <input type="text" name="role" id="role" class="form-control {{ $errors->has('role') ? ' parsley-error' : '' }}" placeholder="Enter link" value="{{(isset($item) ? $item->role : old('role') )}}" >
                @if ($errors->has('role'))
                    <span class="help-block">
                        <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('role') }}</li></ul>
                    </span>
                @endif
            </div>
            <div class="table-responsive mg-b-30">
                <table class="table table-hover mg-b-0">
                    <thead class="bg-dark">
                    <tr>
                        <th class="wd-70 pd-r-0-force">
                            <input id="view" data-group="view" type="checkbox" class="checkbox-view toggle-view checkbox-toggle filled-in" {{(isset($item) && $item->view->count()==$routes->count()) ? 'checked' : '' }}>
                            <label class="ckbox mg-b-0" for="view">View</label>
                        </th>
                        <th class="wd-70 pd-r-0-force">
                            <input id="edit" data-group="edit" type="checkbox" class="checkbox-edit toggle-edit checkbox-toggle filled-in" {{(isset($item) && $item->edit->count()==$routes->count()) ? 'checked' : '' }}>
                            <label class="ckbox mg-b-0" for="edit">Edit</label>
                        </th>
                        <th>Controller</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($routes as $route)
                        <tr class="table-item">
                            <td class="text-center pd-r-0-force">
                                <input id="view_{{$route->id}}" data-id="{{$route->id}}" data-group="view" type="checkbox" class="checkbox-view checkbox filled-in" value="{{$route->id}}"  name="view[]" {{(isset($item) && $item->view->contains($route->id)) ? 'checked' : '' }}>
                                <label for="view_{{$route->id}}" class="ckbox mg-b-0"></label>
                            </td>
                            <td class="text-center pd-r-0-force">
                                <input id="edit_{{$route->id}}" data-id="{{$route->id}}" data-group="edit" type="checkbox" class="checkbox-edit checkbox filled-in" value="{{$route->id}}" name="edit[]" {{(isset($item) && $item->edit->contains($route->id)) ? 'checked' : '' }}>
                                <label for="edit_{{$route->id}}" class="ckbox mg-b-0"></label>
                            </td>
                            <td>{{$route->name}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="form-layout-footer">
            @if($edit)
                <button type="submit" class="btn btn-info mg-r-5"><i class="fa fa-save mg-r-10"></i>Сохранить</button>
            @endif
            <a href="{{ route('admin.roles') }}" class="btn btn-secondary">Назад</a>
        </div>
    </div>
    @if($edit)
        </form>
    @endif
@endsection


@push('scripts')
    <script>
        @if($edit)
            $(document).ready(function() {
                $(".checkbox-toggle").on("change", function() {
                    var group = $(this).data('group');
                    $('.checkbox-'+group).prop("checked", this.checked);

                    if(group=='edit' && this.checked) {
                        $('.checkbox-view').prop("checked", this.checked);
                    } else if(group=='view' && !this.checked) {
                        $('.checkbox-edit').prop("checked", false);
                    }
                });

                $(".checkbox").on("change", function() {
                    var group = $(this).data('group');
                    var allChecked = $('.checkbox[data-group="' + group + '"]:not(:checked)').length == 0;
                    $('.toggle-'+group).prop("checked", allChecked);
                    if(group=='edit' && this.checked) {
                        $('.checkbox[data-id="' + $(this).data('id')+'"]').prop("checked", this.checked);
                        group= 'view';
                        var allChecked = $('.checkbox[data-group="' + group + '"]:not(:checked)').length == 0;
                        $('.toggle-'+group).prop("checked", allChecked);
                    } else if(group=='view' && !this.checked) {
                        if(!this.checked) {}
                        $('.checkbox[data-id="' + $(this).data('id')+'"]').prop("checked", this.checked);
                        $('.toggle-edit').prop("checked", this.checked);
                    }
                });

                $('#table-log').DataTable({
                    order: [[1, 'desc']],
                    'aoColumnDefs': [{
                        'bSortable': false,
                        'aTargets': [-1] /* 1st one, start by the right */
                    }],
                    "ajax": null,
                });
                // Select2
                $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
            });
        @else
            $('input').prop('readonly', true);
        @endif
    </script>

@endpush