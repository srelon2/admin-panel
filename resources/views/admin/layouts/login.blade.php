<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{asset('admin')}}/images/favicon.ico">

    <title>Admin Login</title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="{{asset('admin')}}/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Bootstrap extend-->
    <link rel="stylesheet" href="{{asset('admin')}}/main/css/bootstrap-extend.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('admin')}}/main/css/master_style.css">

    <!-- Fab Admin skins -->
    <link rel="stylesheet" href="{{asset('admin')}}/main/css/skins/_all-skins.css">

</head>
<body class="hold-transition login-page">

<div class="container h-p100">
    <div class="row align-items-center justify-content-md-center h-p100">
        <div class="col-lg-4 col-12">
            @yield('content')
            <!-- /.login-box -->

        </div>
    </div>
</div>


<!-- jQuery 3 -->
<script src="{{asset('admin')}}/assets/vendor_components/jquery/dist/jquery.min.js"></script>

<!-- popper -->
<script src="{{asset('admin')}}/assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap 4.0-->
<script src="{{asset('admin')}}/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

</body>
</html>
