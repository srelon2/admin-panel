<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{asset('admin')}}/images/favicon.ico">

    <title>{{(isset($pagetitle)) ? $pagetitle : 'Admin Panel'}}</title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="{{asset('admin')}}/assets/vendor_components/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="{{asset('admin')}}/assets/vendor_components/bootstrap-switch/switch.css">

    <!-- Bootstrap extend-->
    <link rel="stylesheet" href="{{asset('admin')}}/main/css/bootstrap-extend.css">

    <!-- Data Table-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/assets/vendor_components/datatable/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/assets/vendor_components/select2/dist/css/select2.min.css"/>

    <!-- toast CSS -->
    <link href="{{asset('admin')}}/assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.css" rel="stylesheet">

    <!-- theme style -->
    <link rel="stylesheet" href="{{asset('admin')}}/main/css/master_style.css">

    <!-- Fab Admin skins -->
    <link rel="stylesheet" href="{{asset('admin')}}/main/css/skins/_all-skins.css">

    <!--alerts CSS -->
    <link href="{{asset('admin')}}/assets/vendor_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">

    <!-- Vector CSS -->
    <link href="{{asset('admin')}}/assets/vendor_components/jvectormap/lib2/jquery-jvectormap-2.0.2.css" rel="stylesheet" />

    <!-- Morris charts -->
    <link rel="stylesheet" href="{{asset('admin')}}/assets/vendor_components/morris.js/morris.css">
    <link rel="stylesheet" href="{{asset('admin')}}/assets/vendor_components/morris.js/morris.css">

    <link rel="stylesheet" href="{{asset('admin')}}/css/helpers.css">
    <link rel="stylesheet" href="{{asset('admin')}}/css/styles.css">

</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="{{route('admin.home')}}" class="logo">
            <!-- mini logo -->
            <b class="logo-mini">
                <span class="light-logo"><img src="{{asset('admin')}}/images/logo-light.png" alt="logo"></span>
                <span class="dark-logo"><img src="{{asset('admin')}}/images/logo-dark.png" alt="logo"></span>
            </b>
            <!-- logo-->
            <span class="logo-lg">
		  <img src="{{asset('admin')}}/images/logo-light-text.png" alt="logo" class="light-logo">
	  	  <img src="{{asset('admin')}}/images/logo-dark-text.png" alt="logo" class="dark-logo">
	  </span>
        </a>
        <!-- Header Navbar -->

        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <div>
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                {{--<div class="btn-group d-none d-lg-inline-block mt-5">--}}
                {{--<button class="btn dropdown-toggle mr-10 btn-outline btn-white" type="button" data-toggle="dropdown">Dashboard</button>--}}
                {{--<div class="dropdown-menu">--}}
                {{--<a class="dropdown-item" href="http://html-templates.multipurposethemes.com/bootstrap-4/admin/fab-admin/src/"><i class="fa fa-dashboard w-30"></i>Main Dashboard</a>--}}
                {{--<a class="dropdown-item" href="http://html-templates.multipurposethemes.com/bootstrap-4/admin/fab-admin/ecommerce-dashboard/"><i class="fa fa-shopping-basket w-30"></i>eCommerce Dashboard</a>--}}
                {{--<a class="dropdown-item" href="http://html-templates.multipurposethemes.com/bootstrap-4/admin/fab-admin/hospital-dashboard/"><i class="fa fa-heartbeat w-30"></i>Hospital Dashboard</a>--}}
                {{--<a class="dropdown-item" href="http://html-templates.multipurposethemes.com/bootstrap-4/admin/fab-admin/horizontal-nav/main"><i class="fa fa-bars w-30"></i>Horizontal Nav Dashboard</a>--}}
                {{--<a class="dropdown-item" href="http://html-templates.multipurposethemes.com/bootstrap-4/admin/fab-admin/horizontal-nav/real-estate-dashboard"><i class="fa fa-building w-30"></i>Real Estate Dashboard</a>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <li class="search-box">
                        <a class="nav-link hidden-sm-down" href="javascript:void(0)"><i class="mdi mdi-magnify"></i></a>
                        <form class="app-search" style="display: none;">
                            <input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i class="ti-close"></i></a>
                        </form>
                    </li>

                    @if(count($newMessages))
                        <!-- Messages -->
                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="mdi mdi-email"></i>
                            </a>
                            <ul class="dropdown-menu scale-up">
                                <li class="header">You have {{count($newMessages)}} messages</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu inner-content-div">
                                        @foreach($newMessages as $message)
                                            <li><!-- start message -->
                                                <a href="{{route('admin.support.read',['id'=>$message->id])}}">
                                                    {{--<div class="pull-left">--}}
                                                        {{--<img src="{{asset('admin')}}/images/user2-160x160.jpg" class="rounded-circle" alt="User Image">--}}
                                                    {{--</div>--}}
                                                    <div class="mail-contnet">
                                                        <h4>
                                                            {{$message->email}}
                                                            <small><i class="fa fa-clock-o"></i> {{$message->created_at}}</small>
                                                        </h4>
                                                        <span>{{$message->text}}</span>
                                                    </div>
                                                </a>
                                            </li>
                                        @endforeach
                                        <!-- end message -->
                                    </ul>
                                </li>
                                <li class="footer"><a href="{{route('admin.supports')}}">See all e-Mails</a></li>
                            </ul>
                        </li>
                    @endif
                    <!-- Notifications -->
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="mdi mdi-bell"></i>
                        </a>
                        <ul class="dropdown-menu scale-up">
                            <li class="header">You have 7 notifications</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu inner-content-div">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-aqua"></i> Curabitur id eros quis nunc suscipit blandit.
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-warning text-yellow"></i> Duis malesuada justo eu sapien elementum, in semper diam posuere.
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-red"></i> Donec at nisi sit amet tortor commodo porttitor pretium a erat.
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-shopping-cart text-green"></i> In gravida mauris et nisi
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-user text-red"></i> Praesent eu lacus in libero dictum fermentum.
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-user text-red"></i> Nunc fringilla lorem
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-user text-red"></i> Nullam euismod dolor ut quam interdum, at scelerisque ipsum imperdiet.
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>
                    <!-- Tasks-->
                    <li class="dropdown tasks-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="mdi mdi-message"></i>
                        </a>
                        <ul class="dropdown-menu scale-up">
                            <li class="header">You have 6 tasks</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu inner-content-div">
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <h3>
                                                Lorem ipsum dolor sit amet
                                                <small class="pull-right">30%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-aqua" style="width: 30%" role="progressbar"
                                                     aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">30% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- end task item -->
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <h3>
                                                Vestibulum nec ligula
                                                <small class="pull-right">20%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-danger" style="width: 20%" role="progressbar"
                                                     aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">20% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- end task item -->
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <h3>
                                                Donec id leo ut ipsum
                                                <small class="pull-right">70%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-light-blue" style="width: 70%" role="progressbar"
                                                     aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">70% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- end task item -->
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <h3>
                                                Praesent vitae tellus
                                                <small class="pull-right">40%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-yellow" style="width: 40%" role="progressbar"
                                                     aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">40% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- end task item -->
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <h3>
                                                Nam varius sapien
                                                <small class="pull-right">80%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-red" style="width: 80%" role="progressbar"
                                                     aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">80% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- end task item -->
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <h3>
                                                Nunc fringilla
                                                <small class="pull-right">90%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-primary" style="width: 90%" role="progressbar"
                                                     aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">90% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- end task item -->
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="#">View all tasks</a>
                            </li>
                        </ul>
                    </li>
                    <!-- User Account-->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{asset('admin')}}/images/user5-128x128.jpg" class="user-image rounded-circle" alt="User Image">
                        </a>
                        <ul class="dropdown-menu scale-up">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{asset('admin')}}/images/user5-128x128.jpg" class="float-left rounded-circle" alt="User Image">

                                <p>
                                    Juliya Brus
                                    <small class="mb-5">jb@gmail.com</small>
                                    <a href="#" class="btn btn-danger btn-sm btn-rounded">View Profile</a>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="row no-gutters">
                                    <div class="col-12 text-left">
                                        <a href="#"><i class="ion ion-person"></i> Мой профиль</a>
                                    </div>
                                    <div class="col-12 text-left">
                                        <a href="#"><i class="ion ion-email-unread"></i> Почта</a>
                                    </div>
                                    <div role="separator" class="divider col-12"></div>
                                    <div class="col-12 text-left">
                                        <a href="{{route('admin.settings')}}"><i class="ti-settings"></i> Настройки аккаунта</a>
                                    </div>
                                    <div role="separator" class="divider col-12"></div>
                                    <div class="col-12 text-left">
                                        <a href="{{route('admin.logout')}}"><i class="fa fa-power-off"></i> Logout</a>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-cog"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar-->
        <section class="sidebar">

            <!-- sidebar menu-->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="user-profile treeview">
                    <a href="index.html">
                        <img src="{{asset('admin')}}/images/user5-128x128.jpg" alt="user">
                        <span>Juliya Brus</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="javascript:void()"><i class="fa fa-user mr-5"></i>Мой профиль</a></li>
                        <li><a href="javascript:void()"><i class="fa fa-envelope-open mr-5"></i>Почта</a></li>
                        <li><a href="{{route('admin.settings')}}"><i class="fa fa-cog mr-5"></i>Настройки аккаунта</a></li>
                        <li><a href="{{route('admin.logout')}}"><i class="fa fa-power-off mr-5"></i>Logout</a></li>
                    </ul>
                </li>
                <li class="header nav-small-cap">PERSONAL</li>
                @foreach($navigation as $nav)
                    <li class="{{(isset($nav['isActive'])) ? 'active' : ''}} {{(isset($nav['pages'])) ? 'treeview' : '' }}">
                        <a href="{{isset($nav['url']) ? $nav['url'] : '#'}}">
                            @if(isset($nav['icon']))<i class="{{$nav['icon']}}"></i>@endif <span>{{$nav['title']}}</span>
                            @if(isset($nav['pages']))
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-right pull-right"></i>
                                </span>
                            @endif
                        </a>
                        @if(isset($nav['pages']))
                            <ul class="treeview-menu">
                                @foreach($nav['pages'] as $navSub)
                                    <li class="{{(isset($navSub['isActive'])) ? 'active' : ''}}"><a href="{{$navSub['url']}}">@if(isset($navSub['icon']))<i class="{{$navSub['icon']}}"></i>@endif {{$navSub['title']}}</a></li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            </ul>
        </section>
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 class="clearfix"> {{(isset($pagetitle)) ? $pagetitle : 'Admin Panel' }} <span>@stack('topBtn')</span></h1>
        </section>

        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right d-none d-sm-inline-block">
            <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
                <li class="nav-item">
                    <a class="nav-link" target="_blank" href="{{url('/admin/main/')}}">Template Dashboard</a>
                </li>
            </ul>
        </div>
        &copy; 2018 <a href="https://www.multipurposethemes.com/">Multi-Purpose Themes</a>. All Rights Reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-light">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-cog fa-spin"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Recent Activity</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-birthday-cake bg-danger"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Admin Birthday</h4>

                                <p>Will be July 24th</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-user bg-warning"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Jhone Updated His Profile</h4>

                                <p>New Email : jhone_doe@demo.com</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-envelope-o bg-info"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Disha Joined Mailing List</h4>

                                <p>disha@demo.com</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-file-code-o bg-success"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Code Change</h4>

                                <p>Execution time 15 Days</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

                <h3 class="control-sidebar-heading">Tasks Progress</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Web Design
                                <span class="label label-danger pull-right">40%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: 40%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Update Data
                                <span class="label label-success pull-right">75%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-success" style="width: 75%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Order Process
                                <span class="label label-warning pull-right">89%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-warning" style="width: 89%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Development
                                <span class="label label-primary pull-right">72%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-primary" style="width: 72%"></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">General Settings</h3>

                    <div class="form-group">
                        <input type="checkbox" id="report_panel" class="chk-col-grey" >
                        <label for="report_panel" class="control-sidebar-subheading ">Report panel usage</label>

                        <p>
                            general settings information
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <input type="checkbox" id="allow_mail" class="chk-col-grey" >
                        <label for="allow_mail" class="control-sidebar-subheading ">Mail redirect</label>

                        <p>
                            Other sets of options are available
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <input type="checkbox" id="expose_author" class="chk-col-grey" >
                        <label for="expose_author" class="control-sidebar-subheading ">Expose author name</label>

                        <p>
                            Allow the user to show his name in blog posts
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <h3 class="control-sidebar-heading">Chat Settings</h3>

                    <div class="form-group">
                        <input type="checkbox" id="show_me_online" class="chk-col-grey" >
                        <label for="show_me_online" class="control-sidebar-subheading ">Show me as online</label>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <input type="checkbox" id="off_notifications" class="chk-col-grey" >
                        <label for="off_notifications" class="control-sidebar-subheading ">Turn off notifications</label>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            <a href="javascript:void(0)" class="text-red margin-r-5"><i class="fa fa-trash-o"></i></a>
                            Delete chat history
                        </label>
                    </div>
                    <!-- /.form-group -->
                </form>
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->



<!-- jQuery 3 -->
<script src="{{asset('admin')}}/assets/vendor_components/jquery/dist/jquery.js"></script>

<!-- jQuery UI 1.11.4 -->
<script src="{{asset('admin')}}/assets/vendor_components/jquery-ui/jquery-ui.js"></script>

<!-- popper -->
<script src="{{asset('admin')}}/assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap 4.0-->
<script src="{{asset('admin')}}/assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>

<!-- ChartJS -->
<script src="{{asset('admin')}}/assets/vendor_components/chart.js-master/Chart.min.js"></script>

<!-- Slimscroll -->
<script src="{{asset('admin')}}/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- FastClick -->
<script src="{{asset('admin')}}/assets/vendor_components/fastclick/lib/fastclick.js"></script>

<!-- peity -->
<script src="{{asset('admin')}}/assets/vendor_components/jquery.peity/jquery.peity.js"></script>

<!-- Morris.js charts -->
<script src="{{asset('admin')}}/assets/vendor_components/raphael/raphael.min.js"></script>
<script src="{{asset('admin')}}/assets/vendor_components/morris.js/morris.min.js"></script>

<!-- Fab Admin App -->
<script src="{{asset('admin')}}/main/js/template.js"></script>

<!-- Vector map JavaScript -->
<script src="{{asset('admin')}}/assets/vendor_components/jvectormap/lib2/jquery-jvectormap-2.0.2.min.js"></script>
<script src="{{asset('admin')}}/assets/vendor_components/jvectormap/lib2/jquery-jvectormap-world-mill-en.js"></script>
<script src="{{asset('admin')}}/assets/vendor_components/jvectormap/lib2/jquery-jvectormap-us-aea-en.js"></script>

<!-- select2 -->
<script src="{{asset('admin')}}/assets/vendor_components/select2/dist/js/select2.js"></script>

<!-- This is data table -->
<script src="{{asset('admin')}}/assets/vendor_components/datatable/datatables.js"></script>


<!-- Sweet-Alert  -->
<script src="{{asset('admin')}}/assets/vendor_components/sweetalert/sweetalert.min.js"></script>
<script src="{{asset('admin')}}/assets/vendor_components/sweetalert/jquery.sweet-alert.custom.js"></script>

<!-- toast -->
<script src="{{asset('admin')}}/assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.js"></script>

<script src="{{asset('admin')}}/main/js/demo.js"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
    @if(session('message'))
        $.toast({
        heading: 'Alert. Info!',
        text: "{!! session('message') !!}.",
        position: 'top-right',
        loaderBg: '#1e88e5',
        icon: 'info',
        hideAfter: 3000,
        stack: 6
    });
    @endif
    @if(session('warning'))
        $.toast({
        heading: 'Alert. Warning!',
        text: "{!! session('warning') !!}.",
        position: 'top-right',
        loaderBg: '#ffb22b',
        icon: 'warning',
        hideAfter: 3500,
        stack: 6
    });
    @endif
    @if(session('success'))
        $.toast({
        heading: 'Alert. Success!',
        text: "{!! session('success') !!}.",
        position: 'top-right',
        loaderBg: '#26c6da',
        icon: 'success',
        hideAfter: 3500,
        stack: 6
    });
    @endif
    @if(session('error'))
        $.toast({
        heading: 'Alert. Error!',
        text: "{!! session('error') !!}.",
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: 'error',
        hideAfter: 3500

    });
    @endif

    $(document).ready(function () {
        $('.btn-copy').on('click', function () {
            var input= $(this).parents('.input-group').find('.form-control');
            console.log(input);
            input.select();
            document.execCommand('copy');
            input.append(' ');
            input.val().slice(0, -1);
            $.toast({
                text: "Скопировано.",
                position: 'top-right',
                loaderBg: '#26c6da',
                icon: 'success',
                hideAfter: 750,
                stack: 6
            });
        });
    });
</script>

@stack('scripts')
</body>
</html>
