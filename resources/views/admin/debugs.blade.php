@extends('admin.layouts.layout')

@push('topBtn')
    @if($type=='default')
        <a class="btn btn-success btn-sm mg-l-10" href="{{route('admin.debugs', ['type'=>'new'])}}"><i class="fa fa-th-list mg-r-5"></i> Debugs New</a>
    @else
        <a class="btn btn-success btn-sm mg-l-10" href="{{route('admin.debugs', ['type'=>'default'])}}"><i class="fa fa-th-list mg-r-5"></i> Debugs Default</a>
    @endif
@endpush

@section('content')
    <div class="box-body card">
        @if ($logs === null)
            <div>
                Логов нету
            </div>
        @else
            @include("admin.debugs-".$type)
        @endif
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $('.table-container tr .pull-right ').on('click', function () {
                $('#' + $(this).data('display')).toggle();
            });

            // Setup - add a text input to each footer cell
            $('#table-log tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="'+title+'" />' );
            } );

            table= $('#table-log').DataTable({
                "order": [0, 'desc'],
                "stateSave": true,
                "autoWidth": false,
                "stateSaveCallback": function (settings, data) {
                    window.localStorage.setItem("datatable", JSON.stringify(data));
                },
                "stateLoadCallback": function (settings) {
                    var data = JSON.parse(window.localStorage.getItem("datatable"));
                    if (data) data.start = 0;
                    return data;
                },
                "ajax": null,
            });

            // Apply the search
            table.columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );

            $('.btn-open').on('click', function () {
                $(this).children('.fa').toggleClass('fa-angle-up').toggleClass('fa-angle-down');
                $('#' + $(this).data('display')).toggle();
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
        });
    </script>
@endpush