@extends('admin.layouts.layout')

@push('topBtn')
@if($edit)<a class="btn btn-success btn-sm mg-l-10" href="{{route('admin.languages.info')}}"><i class="fa fa-plus mg-r-10"></i>Создать</a> @endif
@endpush

@section('content')
    <div class="box-body card">
        <div class="table-responsive">
            <table id="datatable1" class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Lang</th>
                    <th>Name</th>
                    <th></th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div><!-- table-wrapper -->
    </div><!-- card -->

@endsection

@push('scripts')
<script>
    $(function(){

        $('#datatable1').DataTable({
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            bPaginate:true,
            responsive: false,
            order: [[0, 'desc']],
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            "ajax": {
                "url": "{{route('admin.languages.search')}}",
            },
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

    });
</script>

@endpush