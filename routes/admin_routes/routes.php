<?php
try {
    Route::middleware('auth')->group(function () {
        $routes = App\Models\Admin\Route::get();
        foreach ($routes as $route) {
            switch ($route->type) {
                case 'post':
                    Route::post($route->link, $route->getController->name . '@' . $route->function)->name($route->name);
                    break;
                default:
                    Route::get($route->link, $route->getController->name . '@' . $route->function)->name($route->name);
            }
        }
    });
} catch (Exception $e) {
    try {
        \Illuminate\Support\Facades\Artisan::call('migrate');
        \Illuminate\Support\Facades\Artisan::call('db:seed');
    } catch (Exception $e) {

    }
}
//Route::post('/roles/search', 'AdminRolesController@search')->name('roles.search');