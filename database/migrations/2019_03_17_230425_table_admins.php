<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableAdmins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('role_id')->default(0);
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('deleted')->default(0);
            $table->integer('active')->default(0);
            $table->integer('google_two_step')->default(0);
            $table->string('google2fa_secret')->nullable();
            $table->integer('email_two_step')->default(0);
            $table->string('email_secret')->nullable();
            $table->integer('confirmed')->default(0);
            $table->string('confirmation_code')->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
