<?php
use Illuminate\Database\Seeder;
class RoutesSeeder extends Seeder
{
    /**
    $data[]= [["type"=> "get",]]$data[]= [["type"=> "get",]]     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('routes')->delete();

        $data[]= [
            'id'=> '1',
            'type'=> 'get',
            'name'=> 'home',
            'link'=> '/',
            'controller'=> '1',
            'function'=> 'index',
            'title'=> 'Dashboard',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/","controller":9,"function":"index","title":"Dashboard","name":"home","description":null}',
        ];
        $data[]= [
            'id'=> '2',
            'type'=> 'get',
            'name'=> 'routes',
            'link'=> '/routes',
            'controller'=> '2',
            'function'=> 'index',
            'title'=> 'Машруты Админ панели',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/routes","controller":"AdminRoutesController","function":"index","title":"\u041c\u0430\u0448\u0440\u0443\u0442\u044b \u0410\u0434\u043c\u0438\u043d \u043f\u0430\u043d\u0435\u043b\u0438","name":"routes","description":null}',
        ];
        $data[]= [
            'id'=> '3',
            'type'=> 'get',
            'name'=> 'routes.info',
            'link'=> '/routes/info/{id?}/{backup?}',
            'controller'=> '2',
            'function'=> 'info',
            'title'=> 'Просмотр Маршрута админ панели',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/routes\/info\/{id?}","controller":"AdminRoutesController","function":"info","title":"\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440 \u041c\u0430\u0440\u0448\u0440\u0443\u0442\u0430 \u0430\u0434\u043c\u0438\u043d \u043f\u0430\u043d\u0435\u043b\u0438","name":"routes.info","description":null}',
        ];
        $data[]= [
            'id'=> '4',
            'type'=> 'post',
            'name'=> 'routes.update',
            'link'=> '/routes/update/{id?}',
            'controller'=> '2',
            'function'=> 'update',
            'title'=> 'Создание маршрута',
            'description'=> '',
            'search'=> '{"type":"post","link":"\/routes\/update\/{id?}","controller":"AdminRoutesController","function":"update","title":"\u0421\u043e\u0437\u0434\u0430\u043d\u0438\u0435 \u043c\u0430\u0440\u0448\u0440\u0443\u0442\u0430","name":"routes.update","description":null}',
        ];
        $data[]= [
            'id'=> '5',
            'type'=> 'post',
            'name'=> 'routes.search',
            'link'=> '/routes/search',
            'controller'=> '2',
            'function'=> 'search',
            'title'=> 'Поиск маршрута',
            'description'=> '',
            'search'=> '{"type":"post","link":"\/routes\/search","controller":"AdminRoutesController","function":"search","title":"\u041f\u043e\u0438\u0441\u043a \u043c\u0430\u0440\u0448\u0440\u0443\u0442\u0430","name":"routes.search","description":null}',
        ];
        $data[]= [
            'id'=> '15',
            'type'=> 'get',
            'name'=> 'administrators',
            'link'=> '/administrators',
            'controller'=> '7',
            'function'=> 'index',
            'title'=> 'Администраторы',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/administrators","controller":"AdminAdministratorsController","function":"index","title":"\u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440\u044b","name":"administrators","description":null}',
        ];
        $data[]= [
            'id'=> '16',
            'type'=> 'get',
            'name'=> 'administrators.info',
            'link'=> '/administrators/info/{id?}/{backup?}',
            'controller'=> '7',
            'function'=> 'info',
            'title'=> 'Просмотр Администратора',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/administrators\/info\/{id?}","controller":"AdminAdministratorsController","function":"info","title":"\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440 \u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440\u0430","name":"administrators.info","description":null}',
        ];
        $data[]= [
            'id'=> '17',
            'type'=> 'post',
            'name'=> 'administrators.update',
            'link'=> '/administrators/update/{id?}',
            'controller'=> '7',
            'function'=> 'update',
            'title'=> 'Создание Администратора',
            'description'=> '',
            'search'=> '{"type":"post","link":"\/administrators\/update\/{id?}","controller":"AdminAdministratorsController","function":"update","title":"\u0421\u043e\u0437\u0434\u0430\u043d\u0438\u0435 \u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440\u0430","name":"administrators.update","description":null}',
        ];
        $data[]= [
            'id'=> '18',
            'type'=> 'get',
            'name'=> 'roles',
            'link'=> '/roles',
            'controller'=> '6',
            'function'=> 'index',
            'title'=> 'Просмотр доступных Ролей Администраторов',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/roles","controller":"AdminRolesController","function":"index","title":"\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440 \u0434\u043e\u0441\u0442\u0443\u043f\u043d\u044b\u0445 \u0420\u043e\u043b\u0435\u0439 \u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440\u043e\u0432","name":"roles","description":null}',
        ];
        $data[]= [
            'id'=> '19',
            'type'=> 'get',
            'name'=> 'roles.info',
            'link'=> '/roles/info/{id?}/{backup?}',
            'controller'=> '6',
            'function'=> 'info',
            'title'=> 'Просмотр Роли Администратора',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/roles\/info\/{id?}","controller":"AdminRolesController","function":"info","title":"\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440 \u0420\u043e\u043b\u0438 \u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440\u0430","name":"roles.info","description":null}',
        ];
        $data[]= [
            'id'=> '20',
            'type'=> 'post',
            'name'=> 'roles.update',
            'link'=> '/roles/update/{id?}',
            'controller'=> '6',
            'function'=> 'update',
            'title'=> 'Создание Роли Администратора',
            'description'=> '',
            'search'=> '{"type":"post","link":"\/roles\/update\/{id?}","controller":"AdminRolesController","function":"update","title":"\u0421\u043e\u0437\u0434\u0430\u043d\u0438\u0435 \u0420\u043e\u043b\u0438 \u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440\u0430","name":"roles.update","description":null}',
        ];
        $data[]= [
            'id'=> '21',
            'type'=> 'post',
            'name'=> 'roles.search',
            'link'=> '/roles/search',
            'controller'=> '6',
            'function'=> 'search',
            'title'=> 'Поиск Роли',
            'description'=> '',
            'search'=> '{"type":"post","link":"\/roles\/search","controller":"AdminRolesController","function":"search","title":"\u041f\u043e\u0438\u0441\u043a \u0420\u043e\u043b\u0438","name":"roles.search","description":null}',
        ];
        $data[]= [
            'id'=> '22',
            'type'=> 'post',
            'name'=> 'administrators.search',
            'link'=> '/administrators/search',
            'controller'=> '7',
            'function'=> 'search',
            'title'=> 'Поиск Администратора',
            'description'=> '',
            'search'=> '{"type":"post","link":"\/administrators\/search","controller":"7","function":"search","title":"\u041f\u043e\u0438\u0441\u043a \u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440\u0430","name":"administrators.search","description":null}',
        ];
        $data[]= [
            'id'=> '23',
            'type'=> 'get',
            'name'=> 'pages',
            'link'=> 'pages',
            'controller'=> '8',
            'function'=> 'index',
            'title'=> 'Просмотр внутренних страниц сайта',
            'description'=> '',
            'search'=> '{"type":"get","link":"pages","controller":8,"function":"index","title":"\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440 \u0432\u043d\u0443\u0442\u0440\u0435\u043d\u043d\u0438\u0445 \u0441\u0442\u0440\u0430\u043d\u0438\u0446 \u0441\u0430\u0439\u0442\u0430","name":"pages","description":null}',
        ];
        $data[]= [
            'id'=> '24',
            'type'=> 'get',
            'name'=> 'pages.info',
            'link'=> '/pages/info/{id?}/{backup?}',
            'controller'=> '8',
            'function'=> 'info',
            'title'=> 'Просмотр страницы сайта',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/pages\/info\/{id?}","controller":"8","function":"info","title":"\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0441\u0430\u0439\u0442\u0430","name":"pages.info","description":null}',
        ];
        $data[]= [
            'id'=> '25',
            'type'=> 'post',
            'name'=> 'pages.update',
            'link'=> '/pages/update/{id?}',
            'controller'=> '8',
            'function'=> 'update',
            'title'=> 'Создание новой страницы сайта',
            'description'=> '',
            'search'=> '{"type":"post","link":"\/pages\/update\/{id?}","controller":"8","function":"update","title":"\u0421\u043e\u0437\u0434\u0430\u043d\u0438\u0435 \u043d\u043e\u0432\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0441\u0430\u0439\u0442\u0430","name":"pages.update","description":null}',
        ];
        $data[]= [
            'id'=> '26',
            'type'=> 'post',
            'name'=> 'pages.search',
            'link'=> '/pages/search',
            'controller'=> '8',
            'function'=> 'search',
            'title'=> 'Поиск Страницы',
            'description'=> '',
            'search'=> '{"type":"post","link":"\/pages\/search","controller":"8","function":"search","title":"\u041f\u043e\u0438\u0441\u043a \u0421\u0442\u0440\u0430\u043d\u0438\u0446\u044b","name":"pages.search","description":null}',
        ];
        $data[]= [
            'id'=> '27',
            'type'=> 'get',
            'name'=> 'languages',
            'link'=> '/languages',
            'controller'=> '9',
            'function'=> 'index',
            'title'=> 'Просмотр языков сайта',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/languages","controller":9,"function":"index","title":"\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440 \u044f\u0437\u044b\u043a\u043e\u0432 \u0441\u0430\u0439\u0442\u0430","name":"languages","description":null}',
        ];
        $data[]= [
            'id'=> '28',
            'type'=> 'post',
            'name'=> 'languages.update',
            'link'=> '/languages/update/{id?}',
            'controller'=> '9',
            'function'=> 'update',
            'title'=> 'Создание нового языка',
            'description'=> '',
            'search'=> '{"type":"post","link":"\/languages\/update\/{id?}","controller":"9","function":"update","title":"\u0421\u043e\u0437\u0434\u0430\u043d\u0438\u0435 \u043d\u043e\u0432\u043e\u0433\u043e \u044f\u0437\u044b\u043a\u0430","name":"languages.update","description":null}',
        ];
        $data[]= [
            'id'=> '29',
            'type'=> 'get',
            'name'=> 'languages.info',
            'link'=> '/languages/info/{id?}/{backup?}',
            'controller'=> '9',
            'function'=> 'info',
            'title'=> 'Просмотр перевода',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/languages\/info\/{id?}","controller":"9","function":"info","title":"\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440 \u043f\u0435\u0440\u0435\u0432\u043e\u0434\u0430","name":"languages.info","description":null}',
        ];
        $data[]= [
            'id'=> '30',
            'type'=> 'post',
            'name'=> 'languages.search',
            'link'=> '/languages/search',
            'controller'=> '9',
            'function'=> 'search',
            'title'=> 'Поиск перевода',
            'description'=> '',
            'search'=> '{"type":"post","link":"\/languages\/search","controller":"9","function":"search","title":"\u041f\u043e\u0438\u0441\u043a \u043f\u0435\u0440\u0435\u0432\u043e\u0434\u0430","name":"languages.search","description":null}',
        ];
        $data[]= [
            'id'=> '31',
            'type'=> 'get',
            'name'=> 'supports',
            'link'=> 'supports/{type?}/{table?}/{id?}',
            'controller'=> '10',
            'function'=> 'index',
            'title'=> 'Почтовый ящик',
            'description'=> '',
            'search'=> '{"type":"get","link":"supports\/{type?}\/{table?}\/{id?}","controller":"10","function":"index","title":"\u041f\u043e\u0447\u0442\u043e\u0432\u044b\u0439 \u044f\u0449\u0438\u043a","name":"supports","description":null}',
        ];
        $data[]= [
            'id'=> '32',
            'type'=> 'post',
            'name'=> 'supports.search',
            'link'=> '/supports/search/{type?}/{table?}/{id?}',
            'controller'=> '10',
            'function'=> 'search',
            'title'=> 'Поиск сообщения',
            'description'=> '',
            'search'=> '{"type":"post","link":"\/supports\/search\/{type?}\/{table?}\/{id?}","controller":"10","function":"search","title":"\u041f\u043e\u0438\u0441\u043a \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f","name":"supports.search","description":null}',
        ];
        $data[]= [
            'id'=> '33',
            'type'=> 'post',
            'name'=> 'support.action',
            'link'=> '/support/action',
            'controller'=> '10',
            'function'=> 'action',
            'title'=> 'Действия для почтового ящика',
            'description'=> '',
            'search'=> '{"type":"post","link":"\/support\/action","controller":"11","function":"action","title":"\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u044f \u0434\u043b\u044f \u043f\u043e\u0447\u0442\u043e\u0432\u043e\u0433\u043e \u044f\u0449\u0438\u043a\u0430","name":"support.action","description":null}',
        ];
        $data[]= [
            'id'=> '34',
            'type'=> 'get',
            'name'=> 'debugs',
            'link'=> '/debugs/{type?}',
            'controller'=> '11',
            'function'=> 'debugs',
            'title'=> 'Debug',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/debugs\/{type?}","controller":"11","function":"debugs","title":"Debug","name":"debugs","description":null}',
        ];
        $data[]= [
            'id'=> '35',
            'type'=> 'get',
            'name'=> 'logs',
            'link'=> '/logs',
            'controller'=> '11',
            'function'=> 'index',
            'title'=> 'Просмотр логов администраторов',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/logs","controller":"12","function":"index","title":"\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440 \u043b\u043e\u0433\u043e\u0432 \u0430\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440\u043e\u0432","name":"logs","description":null}',
        ];
        $data[]= [
            'id'=> '36',
            'type'=> 'get',
            'name'=> 'support.compose',
            'link'=> '/support/сompose/{id?}/{answer?}',
            'controller'=> '10',
            'function'=> 'compose',
            'title'=> 'Compose New Message',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/support\/\u0441ompose\/{id?}\/{answer?}","controller":"10","function":"compose","title":"Compose New Message","name":"support.compose","description":null}',
        ];
        $data[]= [
            'id'=> '37',
            'type'=> 'post',
            'name'=> 'support.send',
            'link'=> 'support/send/{id?}/{type?}',
            'controller'=> '10',
            'function'=> 'send',
            'title'=> 'Отправка сообщения пользователю',
            'description'=> '',
            'search'=> '{"type":"post","link":"support\/send\/{id?}\/{type?}","controller":"10","function":"send","title":"\u041e\u0442\u043f\u0440\u0430\u0432\u043a\u0430 \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044e","name":"support.send","description":null}',
        ];
        $data[]= [
            'id'=> '38',
            'type'=> 'get',
            'name'=> 'support.read',
            'link'=> 'support/read/{id}/{table?}',
            'controller'=> '10',
            'function'=> 'read',
            'title'=> 'Просмотр сообщения',
            'description'=> '',
            'search'=> '{"type":"get","link":"support\/read\/{id}\/{table?}","controller":"10","function":"read","title":"\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440 \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f","name":"support.read","description":null}',
        ];
        $data[]= [
            'id'=> '39',
            'type'=> 'get',
            'name'=> 'support.item.action',
            'link'=> '/support/action/{action}/{id}/{table?}',
            'controller'=> '10',
            'function'=> 'itemAction',
            'title'=> 'Действие с сообщением',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/support\/action\/{action}\/{id}\/{table?}","controller":"10","function":"itemAction","title":"\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0435 \u0441 \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435\u043c","name":"support.item.action","description":null}',
        ];
        $data[]= [
            'id'=> '40',
            'type'=> 'get',
            'name'=> 'routes.refresh',
            'link'=> '/routes/refresh',
            'controller'=> '2',
            'function'=> 'refresh',
            'title'=> 'Обновить посев данных для маршрутов',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/routes\/refresh","controller":"2","function":"refresh","title":"\u041e\u0431\u043d\u043e\u0432\u0438\u0442\u044c \u043f\u043e\u0441\u0435\u0432 \u0434\u0430\u043d\u043d\u044b\u0445 \u0434\u043b\u044f \u043c\u0430\u0440\u0448\u0440\u0443\u0442\u043e\u0432","name":"routes.refresh","description":null}',
        ];
        $data[]= [
            'id'=> '41',
            'type'=> 'get',
            'name'=> 'routes.seeds',
            'link'=> '/routes/seeds',
            'controller'=> '2',
            'function'=> 'seeds',
            'title'=> 'Скачать посев данных для маршрутов',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/routes\/seeds","controller":"2","function":"seeds","title":"\u0421\u043a\u0430\u0447\u0430\u0442\u044c \u043f\u043e\u0441\u0435\u0432 \u0434\u0430\u043d\u043d\u044b\u0445 \u0434\u043b\u044f \u043c\u0430\u0440\u0448\u0440\u0443\u0442\u043e\u0432 sdadsa","name":"routes.seeds","description":null}',
        ];
        $data[]= [
            'id'=> '42',
            'type'=> 'get',
            'name'=> 'settings',
            'link'=> '/settings/{type?}',
            'controller'=> '12',
            'function'=> 'index',
            'title'=> 'Настройки администратора',
            'description'=> '',
            'search'=> '{"type":"get","link":"settings\/{?type}","controller":"12","function":"index","title":"\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0430\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440\u0430","name":"settings","description":null}',
        ];
        $data[]= [
            'id'=> '43',
            'type'=> 'post',
            'name'=> 'settings.update',
            'link'=> '/settings/update',
            'controller'=> '12',
            'function'=> 'update',
            'title'=> 'Обновление профиля администратора',
            'description'=> '',
            'search'=> '{"type":"post","link":"\/settings\/update","controller":"12","function":"update","title":"\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u0435 \u043f\u0440\u043e\u0444\u0438\u043b\u044f \u0430\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440\u0430","name":"settings.update","description":null}',
        ];
        $data[]= [
            'id'=> '44',
            'type'=> 'post',
            'name'=> 'settings.safery',
            'link'=> 'settings/safery/{type?}',
            'controller'=> '12',
            'function'=> 'safery',
            'title'=> 'Изменения настроек безопастности',
            'description'=> '',
            'search'=> '{"type":"post","link":"settings\/safery\/{type?}","controller":"12","function":"safery","title":"\u0418\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f \u043d\u0430\u0441\u0442\u0440\u043e\u0435\u043a \u0431\u0435\u0437\u043e\u043f\u0430\u0441\u0442\u043d\u043e\u0441\u0442\u0438","name":"settings.safery","description":null}',
        ];
        $data[]= [
            'id'=> '45',
            'type'=> 'get',
            'name'=> 'setting.secret',
            'link'=> '/setting/secret/{type?}/{id?}',
            'controller'=> '12',
            'function'=> 'secret',
            'title'=> 'Выслать код для активации Email',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/setting\/secret\/{type?}\/{id?}","controller":"12","function":"secret","title":"\u0412\u044b\u0441\u043b\u0430\u0442\u044c \u043a\u043e\u0434 \u0434\u043b\u044f \u0430\u043a\u0442\u0438\u0432\u0430\u0446\u0438\u0438 Email","name":"setting.secret","description":null}',
        ];
        $data[]= [
            'id'=> '46',
            'type'=> 'get',
            'name'=> 'pages.item.action',
            'link'=> 'pages/action/{action}/{id?}',
            'controller'=> '8',
            'function'=> 'itemAction',
            'title'=> 'Удаление страницы',
            'description'=> '',
            'search'=> '{"type":"get","link":"pages\/action\/{action}\/{id?}","controller":"8","function":"itemAction","title":"\u0423\u0434\u0430\u043b\u0435\u043d\u0438\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b","name":"pages.item.action","description":null}',
        ];
        $data[]= [
            'id'=> '47',
            'type'=> 'get',
            'name'=> 'routes.item.action',
            'link'=> 'routes/action/{action}/{id?}',
            'controller'=> '2',
            'function'=> 'itemAction',
            'title'=> 'Удаление маршрута',
            'description'=> '',
            'search'=> '{"type":"get","link":"routes\/action\/{action}\/{id?}","controller":"2","function":"itemAction","title":"\u0423\u0434\u0430\u043b\u0435\u043d\u0438\u0435 \u043c\u0430\u0440\u0448\u0440\u0443\u0442\u0430","name":"routes.item.action","description":null}',
        ];
        $data[]= [
            'id'=> '49',
            'type'=> 'get',
            'name'=> 'roles.item.action',
            'link'=> 'roles/action/{action}/{id?}',
            'controller'=> '6',
            'function'=> 'itemAction',
            'title'=> 'Совершить действие с Ролью',
            'description'=> '',
            'search'=> '{"type":"get","link":"roles\/action\/{action}\/{id?}","controller":"6","function":"itemAction","title":"\u0421\u043e\u0432\u0435\u0440\u0448\u0438\u0442\u044c \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0435 \u0441 \u0420\u043e\u043b\u044c\u044e","name":"roles.item.action","description":null}',
        ];
        $data[]= [
            'id'=> '50',
            'type'=> 'get',
            'name'=> 'administrators.item.action',
            'link'=> 'administrators/action/{action}/{id?}',
            'controller'=> '7',
            'function'=> 'itemAction',
            'title'=> 'Совершить действие с Администратором',
            'description'=> '',
            'search'=> '{"type":"get","link":"administrators\/action\/{action}\/{id?}","controller":"7","function":"itemAction","title":"\u0421\u043e\u0432\u0435\u0440\u0448\u0438\u0442\u044c \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0435 \u0441 \u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440\u043e\u043c","name":"administrators.item.action","description":null}',
        ];
        $data[]= [
            'id'=> '52',
            'type'=> 'get',
            'name'=> 'administrators.logout.other',
            'link'=> 'administrators/logout/other/{id?}',
            'controller'=> '7',
            'function'=> 'logoutOther',
            'title'=> 'Завершить остальные сеансы администратора',
            'description'=> '',
            'search'=> '{"type":"get","link":"administrators\/logout\/other\/{id?}","controller":"7","function":"logoutOther","title":"\u0417\u0430\u0432\u0435\u0440\u0448\u0438\u0442\u044c \u043e\u0441\u0442\u0430\u043b\u044c\u043d\u044b\u0435 \u0441\u0435\u0430\u043d\u0441\u044b \u0430\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440\u0430","name":"administrators.logout.other","description":null}',
        ];
        $data[]= [
            'id'=> '53',
            'type'=> 'post',
            'name'=> 'languages.edit',
            'link'=> 'languages/edit/{id}/{type?}',
            'controller'=> '9',
            'function'=> 'edit',
            'title'=> 'Изменение перевода',
            'description'=> '',
            'search'=> '{"type":"post","link":"languages\/edit\/{id}\/{type?}","controller":"9","function":"edit","title":"\u0418\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u0435 \u043f\u0435\u0440\u0435\u0432\u043e\u0434\u0430","name":"languages.edit","description":null}',
        ];
        $data[]= [
            'id'=> '54',
            'type'=> 'get',
            'name'=> 'export',
            'link'=> 'export/{table}/{format?}/{id?}',
            'controller'=> '1',
            'function'=> 'export',
            'title'=> 'Експорт таблицы',
            'description'=> '',
            'search'=> '{"type":"get","link":"export\/{table}\/{format?}\/{id?}","controller":"1","function":"export","title":"\u0415\u043a\u0441\u043f\u043e\u0440\u0442 \u0442\u0430\u0431\u043b\u0438\u0446\u044b","name":"export","description":null}',
        ];
        $data[]= [
            'id'=> '55',
            'type'=> 'post',
            'name'=> 'import',
            'link'=> 'import/{table}/{format?}/{id?}',
            'controller'=> '1',
            'function'=> 'import',
            'title'=> 'Импорт таблицы',
            'description'=> '',
            'search'=> '{"type":"post","link":"import\/{table}\/{format?}\/{id?}","controller":"1","function":"import","title":"\u0418\u043c\u043f\u043e\u0440\u0442 \u0442\u0430\u0431\u043b\u0438\u0446\u044b","name":"import","description":null}',
        ];
        $data[]= [
            'id'=> '56',
            'type'=> 'post',
            'name'=> 'languages.import',
            'link'=> 'languages/import/{id}',
            'controller'=> '9',
            'function'=> 'import',
            'title'=> 'Импорт перевода',
            'description'=> '',
            'search'=> '{"type":"post","link":"languages\/import\/{id}","controller":"9","function":"import","title":"\u0418\u043c\u043f\u043e\u0440\u0442 \u043f\u0435\u0440\u0435\u0432\u043e\u0434\u0430","name":"languages.import","description":null}',
        ];
        $data[]= [
            'id'=> '57',
            'type'=> 'post',
            'name'=> 'languages.created.key',
            'link'=> '/languages/created/key',
            'controller'=> '9',
            'function'=> 'createdKey',
            'title'=> 'Создание нового ключа перевода',
            'description'=> '',
            'search'=> '{"type":"post","link":"\/languages\/created\/key","controller":"9","function":"createdKey","title":"\u0421\u043e\u0437\u0434\u0430\u043d\u0438\u0435 \u043d\u043e\u0432\u043e\u0433\u043e \u043a\u043b\u044e\u0447\u0430 \u043f\u0435\u0440\u0435\u0432\u043e\u0434\u0430","name":"languages.created.key","description":null}',
        ];
        $data[]= [
            'id'=> '58',
            'type'=> 'get',
            'name'=> 'users',
            'link'=> '/users',
            'controller'=> '13',
            'function'=> 'index',
            'title'=> 'Просмотр пользователей сайта',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/users","controller":13,"function":"index","title":"\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u0435\u0439 \u0441\u0430\u0439\u0442\u0430","name":"users","description":null}',
        ];
        $data[]= [
            'id'=> '59',
            'type'=> 'post',
            'name'=> 'users.update',
            'link'=> '/users/update/{id?}',
            'controller'=> '13',
            'function'=> 'update',
            'title'=> 'Создание пользователя',
            'description'=> '',
            'search'=> '{"type":"post","link":"\/users\/update\/{id?}","controller":"13","function":"update","title":"\u0421\u043e\u0437\u0434\u0430\u043d\u0438\u0435 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f","name":"users.update","description":null}',
        ];
        $data[]= [
            'id'=> '60',
            'type'=> 'get',
            'name'=> 'users.info',
            'link'=> '/users/info/{id?}',
            'controller'=> '13',
            'function'=> 'info',
            'title'=> 'Просмотр пользователя сайта',
            'description'=> '',
            'search'=> '{"type":"get","link":"\/users\/info\/{id?}","controller":"13","function":"info","title":"\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f \u0441\u0430\u0439\u0442\u0430","name":"users.info","description":null}',
        ];
        $data[]= [
            'id'=> '61',
            'type'=> 'post',
            'name'=> 'users.search',
            'link'=> '/users/search',
            'controller'=> '13',
            'function'=> 'search',
            'title'=> 'Поиск пользователя сайта',
            'description'=> '',
            'search'=> '{"type":"post","link":"\/users\/search","controller":"13","function":"search","title":"\u041f\u043e\u0438\u0441\u043a \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f \u0441\u0430\u0439\u0442\u0430","name":"users.search","description":null}',
        ];
        $data[]= [
            'id'=> '62',
            'type'=> 'get',
            'name'=> 'users.item.action',
            'link'=> 'users/action/{action}/{id?}',
            'controller'=> '13',
            'function'=> 'itemAction',
            'title'=> 'Совершить действие с пользователем',
            'description'=> '',
            'search'=> '{"type":"get","link":"users\/action\/{action}\/{id?}","controller":"13","function":"itemAction","title":"\u0421\u043e\u0432\u0435\u0440\u0448\u0438\u0442\u044c \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0435 \u0441 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u0435\u043c","name":"users.item.action","description":null}',
        ];
        DB::table("routes")->insert($data);
    }
}