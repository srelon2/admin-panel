<?php
use Illuminate\Database\Seeder;
class ControllersSeeder extends Seeder
{
    /**
    $data[]= [["type"=> "get",]]$data[]= [["type"=> "get",]]     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('controllers')->delete();

        $data[]= [
            'id'=> '1',
            'name'=> 'AdminController',
        ];
        $data[]= [
            'id'=> '2',
            'name'=> 'AdminRoutesController',
        ];
        $data[]= [
            'id'=> '6',
            'name'=> 'AdminRolesController',
        ];
        $data[]= [
            'id'=> '7',
            'name'=> 'AdminAdministratorsController',
        ];
        $data[]= [
            'id'=> '8',
            'name'=> 'AdminPagesController',
        ];
        $data[]= [
            'id'=> '9',
            'name'=> 'AdminLanguagesController',
        ];
        $data[]= [
            'id'=> '10',
            'name'=> 'AdminSupportsController',
        ];
        $data[]= [
            'id'=> '11',
            'name'=> 'AdminLogsController',
        ];
        $data[]= [
            'id'=> '12',
            'name'=> 'AdminSettingsController',
        ];
        $data[]= [
            'id'=> '13',
            'name'=> 'AdminUsersController',
        ];
        DB::table("controllers")->insert($data);
    }
}