<?php
use Illuminate\Database\Seeder;
class LanguagesSeeder extends Seeder
{
    /**
    $data[]= [["type"=> "get",]]$data[]= [["type"=> "get",]]     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->delete();

        $data[]= [
            'key'=> 'ru',
            'name'=> 'Русский',
        ];
        DB::table('languages')->insert($data);
    }
}